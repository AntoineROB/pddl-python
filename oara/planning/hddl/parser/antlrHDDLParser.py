# Generated from antlrHDDL.g4 by ANTLR 4.8
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3L")
        buf.write("\u037f\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31")
        buf.write("\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36")
        buf.write("\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t")
        buf.write("&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\4-\t-\4.\t.\4")
        buf.write("/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t\64")
        buf.write("\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t")
        buf.write(";\4<\t<\4=\t=\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\t")
        buf.write("D\4E\tE\4F\tF\4G\tG\4H\tH\4I\tI\4J\tJ\4K\tK\4L\tL\4M\t")
        buf.write("M\4N\tN\4O\tO\4P\tP\4Q\tQ\4R\tR\4S\tS\3\2\3\2\5\2\u00a9")
        buf.write("\n\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3\u00b2\n\3\3\3\5\3")
        buf.write("\u00b5\n\3\3\3\5\3\u00b8\n\3\3\3\5\3\u00bb\n\3\3\3\5\3")
        buf.write("\u00be\n\3\3\3\7\3\u00c1\n\3\f\3\16\3\u00c4\13\3\3\3\7")
        buf.write("\3\u00c7\n\3\f\3\16\3\u00ca\13\3\3\3\7\3\u00cd\n\3\f\3")
        buf.write("\16\3\u00d0\13\3\3\3\3\3\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3")
        buf.write("\6\6\6\u00dc\n\6\r\6\16\6\u00dd\3\7\3\7\3\7\3\7\3\7\3")
        buf.write("\b\7\b\u00e6\n\b\f\b\16\b\u00e9\13\b\3\b\3\b\3\b\3\b\3")
        buf.write("\b\5\b\u00f0\n\b\3\t\6\t\u00f3\n\t\r\t\16\t\u00f4\3\n")
        buf.write("\3\n\3\n\3\n\3\n\3\13\3\13\3\13\6\13\u00ff\n\13\r\13\16")
        buf.write("\13\u0100\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3")
        buf.write("\r\3\r\3\r\5\r\u0110\n\r\6\r\u0112\n\r\r\r\16\r\u0113")
        buf.write("\3\r\3\r\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17")
        buf.write("\3\17\3\17\5\17\u0123\n\17\3\17\3\17\5\17\u0127\n\17\3")
        buf.write("\17\3\17\3\20\3\20\3\21\3\21\3\21\3\21\3\21\3\21\3\21")
        buf.write("\3\21\3\21\3\21\3\21\7\21\u0138\n\21\f\21\16\21\u013b")
        buf.write("\13\21\3\21\3\21\3\21\5\21\u0140\n\21\3\21\3\21\5\21\u0144")
        buf.write("\n\21\3\21\3\21\3\22\3\22\5\22\u014a\n\22\3\22\3\22\5")
        buf.write("\22\u014e\n\22\3\22\3\22\5\22\u0152\n\22\3\22\3\22\5\22")
        buf.write("\u0156\n\22\3\22\3\22\3\23\3\23\3\24\3\24\3\24\3\24\3")
        buf.write("\24\3\24\6\24\u0162\n\24\r\24\16\24\u0163\3\24\3\24\5")
        buf.write("\24\u0168\n\24\3\25\3\25\3\25\7\25\u016d\n\25\f\25\16")
        buf.write("\25\u0170\13\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\7\25")
        buf.write("\u0179\n\25\f\25\16\25\u017c\13\25\3\25\3\25\3\25\5\25")
        buf.write("\u0181\n\25\3\26\3\26\3\27\3\27\3\27\3\27\3\27\3\27\6")
        buf.write("\27\u018b\n\27\r\27\16\27\u018c\3\27\3\27\5\27\u0191\n")
        buf.write("\27\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30")
        buf.write("\3\30\3\30\5\30\u019f\n\30\3\31\3\31\3\31\3\31\3\31\3")
        buf.write("\31\6\31\u01a7\n\31\r\31\16\31\u01a8\3\31\3\31\5\31\u01ad")
        buf.write("\n\31\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32")
        buf.write("\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32")
        buf.write("\3\32\3\32\3\32\3\32\3\32\3\32\3\32\5\32\u01cb\n\32\3")
        buf.write("\33\3\33\3\33\3\33\3\33\3\33\6\33\u01d3\n\33\r\33\16\33")
        buf.write("\u01d4\3\33\3\33\5\33\u01d9\n\33\3\34\3\34\3\34\3\34\3")
        buf.write("\34\3\34\3\35\3\35\3\35\3\35\3\36\3\36\3\36\3\36\3\36")
        buf.write("\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36")
        buf.write("\5\36\u01f5\n\36\3\37\3\37\3\37\3 \3 \3 \6 \u01fd\n \r")
        buf.write(" \16 \u01fe\3 \3 \3!\3!\3!\6!\u0206\n!\r!\16!\u0207\3")
        buf.write("!\3!\3\"\3\"\3\"\3\"\3\"\3#\3#\3#\3#\3#\3#\3$\3$\3$\3")
        buf.write("$\3$\3$\3$\3$\3%\3%\3%\3%\3%\3%\3%\3%\3&\3&\3&\3&\3&\3")
        buf.write("\'\3\'\3\'\3\'\3\'\3(\3(\3(\3(\3(\3)\3)\3)\3)\3)\3*\3")
        buf.write("*\3*\3*\3*\3+\3+\3+\3+\3+\3+\3,\3,\3,\3,\3,\3,\3-\3-\3")
        buf.write("-\3-\3-\3-\3.\3.\3.\3.\3.\3.\5.\u0258\n.\3/\3/\3/\3\60")
        buf.write("\3\60\3\60\6\60\u0260\n\60\r\60\16\60\u0261\3\60\3\60")
        buf.write("\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\62\3\62\3\62")
        buf.write("\3\62\3\62\3\62\3\63\3\63\5\63\u0276\n\63\3\64\3\64\3")
        buf.write("\64\3\64\3\64\3\65\3\65\3\65\3\65\3\65\3\65\3\66\3\66")
        buf.write("\3\67\3\67\3\67\3\67\7\67\u0289\n\67\f\67\16\67\u028c")
        buf.write("\13\67\3\67\3\67\5\67\u0290\n\67\38\38\38\38\38\38\38")
        buf.write("\38\38\38\38\68\u029d\n8\r8\168\u029e\38\38\38\38\38\3")
        buf.write("8\38\38\58\u02a9\n8\39\39\39\59\u02ae\n9\3:\3:\3;\3;\3")
        buf.write(";\7;\u02b5\n;\f;\16;\u02b8\13;\3;\3;\3<\3<\3=\3=\3=\5")
        buf.write("=\u02c1\n=\3>\7>\u02c4\n>\f>\16>\u02c7\13>\3?\7?\u02ca")
        buf.write("\n?\f?\16?\u02cd\13?\3@\6@\u02d0\n@\r@\16@\u02d1\3@\3")
        buf.write("@\3@\3A\3A\3A\3A\3B\6B\u02dc\nB\rB\16B\u02dd\3B\3B\3B")
        buf.write("\3C\3C\3D\3D\3D\3D\6D\u02e9\nD\rD\16D\u02ea\3D\3D\5D\u02ef")
        buf.write("\nD\3E\3E\3F\3F\3F\5F\u02f6\nF\3G\3G\3G\7G\u02fb\nG\f")
        buf.write("G\16G\u02fe\13G\3G\3G\3H\3H\3I\3I\3I\3I\3I\3I\3I\3I\3")
        buf.write("I\3I\3I\5I\u030f\nI\3I\5I\u0312\nI\3I\5I\u0315\nI\3I\3")
        buf.write("I\5I\u0319\nI\3I\5I\u031c\nI\3I\5I\u031f\nI\3I\3I\3J\3")
        buf.write("J\3J\3J\3J\3K\3K\3K\7K\u032b\nK\fK\16K\u032e\13K\3K\3")
        buf.write("K\3L\3L\5L\u0334\nL\3M\3M\3M\3M\3M\3N\3N\3N\3N\3N\3O\3")
        buf.write("O\3O\3O\3O\3O\3O\5O\u0347\nO\3O\3O\3P\3P\3P\3P\3P\3P\3")
        buf.write("Q\3Q\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\6R\u035d\nR\rR\16R")
        buf.write("\u035e\3R\3R\3R\3R\3R\5R\u0366\nR\3R\3R\3R\3R\3R\3R\3")
        buf.write("R\7R\u036f\nR\fR\16R\u0372\13R\3R\3R\3R\3R\5R\u0378\n")
        buf.write("R\3S\3S\3S\3S\3S\3S\2\2T\2\4\6\b\n\f\16\20\22\24\26\30")
        buf.write("\32\34\36 \"$&(*,.\60\62\64\668:<>@BDFHJLNPRTVXZ\\^`b")
        buf.write("dfhjlnprtvxz|~\u0080\u0082\u0084\u0086\u0088\u008a\u008c")
        buf.write("\u008e\u0090\u0092\u0094\u0096\u0098\u009a\u009c\u009e")
        buf.write("\u00a0\u00a2\u00a4\2\13\3\2\23\26\3\2\27\30\3\2\32\33")
        buf.write("\3\2\37\"\3\2\60\64\3\2\66\67\3\2HI\3\2@A\3\2CD\2\u0398")
        buf.write("\2\u00a8\3\2\2\2\4\u00aa\3\2\2\2\6\u00d3\3\2\2\2\b\u00d5")
        buf.write("\3\2\2\2\n\u00db\3\2\2\2\f\u00df\3\2\2\2\16\u00ef\3\2")
        buf.write("\2\2\20\u00f2\3\2\2\2\22\u00f6\3\2\2\2\24\u00fb\3\2\2")
        buf.write("\2\26\u0104\3\2\2\2\30\u0109\3\2\2\2\32\u0117\3\2\2\2")
        buf.write("\34\u011b\3\2\2\2\36\u012a\3\2\2\2 \u012c\3\2\2\2\"\u0149")
        buf.write("\3\2\2\2$\u0159\3\2\2\2&\u0167\3\2\2\2(\u0180\3\2\2\2")
        buf.write("*\u0182\3\2\2\2,\u0190\3\2\2\2.\u019e\3\2\2\2\60\u01ac")
        buf.write("\3\2\2\2\62\u01ca\3\2\2\2\64\u01d8\3\2\2\2\66\u01da\3")
        buf.write("\2\2\28\u01e0\3\2\2\2:\u01f4\3\2\2\2<\u01f6\3\2\2\2>\u01f9")
        buf.write("\3\2\2\2@\u0202\3\2\2\2B\u020b\3\2\2\2D\u0210\3\2\2\2")
        buf.write("F\u0216\3\2\2\2H\u021e\3\2\2\2J\u0226\3\2\2\2L\u022b\3")
        buf.write("\2\2\2N\u0230\3\2\2\2P\u0235\3\2\2\2R\u023a\3\2\2\2T\u023f")
        buf.write("\3\2\2\2V\u0245\3\2\2\2X\u024b\3\2\2\2Z\u0257\3\2\2\2")
        buf.write("\\\u0259\3\2\2\2^\u025c\3\2\2\2`\u0265\3\2\2\2b\u026d")
        buf.write("\3\2\2\2d\u0275\3\2\2\2f\u0277\3\2\2\2h\u027c\3\2\2\2")
        buf.write("j\u0282\3\2\2\2l\u028f\3\2\2\2n\u02a8\3\2\2\2p\u02ad\3")
        buf.write("\2\2\2r\u02af\3\2\2\2t\u02b1\3\2\2\2v\u02bb\3\2\2\2x\u02c0")
        buf.write("\3\2\2\2z\u02c5\3\2\2\2|\u02cb\3\2\2\2~\u02cf\3\2\2\2")
        buf.write("\u0080\u02d6\3\2\2\2\u0082\u02db\3\2\2\2\u0084\u02e2\3")
        buf.write("\2\2\2\u0086\u02ee\3\2\2\2\u0088\u02f0\3\2\2\2\u008a\u02f5")
        buf.write("\3\2\2\2\u008c\u02f7\3\2\2\2\u008e\u0301\3\2\2\2\u0090")
        buf.write("\u0303\3\2\2\2\u0092\u0322\3\2\2\2\u0094\u0327\3\2\2\2")
        buf.write("\u0096\u0333\3\2\2\2\u0098\u0335\3\2\2\2\u009a\u033a\3")
        buf.write("\2\2\2\u009c\u033f\3\2\2\2\u009e\u034a\3\2\2\2\u00a0\u0350")
        buf.write("\3\2\2\2\u00a2\u0377\3\2\2\2\u00a4\u0379\3\2\2\2\u00a6")
        buf.write("\u00a9\5\4\3\2\u00a7\u00a9\5\u0090I\2\u00a8\u00a6\3\2")
        buf.write("\2\2\u00a8\u00a7\3\2\2\2\u00a9\3\3\2\2\2\u00aa\u00ab\7")
        buf.write("\3\2\2\u00ab\u00ac\7\4\2\2\u00ac\u00ad\7\3\2\2\u00ad\u00ae")
        buf.write("\7\5\2\2\u00ae\u00af\5\6\4\2\u00af\u00b1\7\6\2\2\u00b0")
        buf.write("\u00b2\5\b\5\2\u00b1\u00b0\3\2\2\2\u00b1\u00b2\3\2\2\2")
        buf.write("\u00b2\u00b4\3\2\2\2\u00b3\u00b5\5\f\7\2\u00b4\u00b3\3")
        buf.write("\2\2\2\u00b4\u00b5\3\2\2\2\u00b5\u00b7\3\2\2\2\u00b6\u00b8")
        buf.write("\5\22\n\2\u00b7\u00b6\3\2\2\2\u00b7\u00b8\3\2\2\2\u00b8")
        buf.write("\u00ba\3\2\2\2\u00b9\u00bb\5\24\13\2\u00ba\u00b9\3\2\2")
        buf.write("\2\u00ba\u00bb\3\2\2\2\u00bb\u00bd\3\2\2\2\u00bc\u00be")
        buf.write("\5\30\r\2\u00bd\u00bc\3\2\2\2\u00bd\u00be\3\2\2\2\u00be")
        buf.write("\u00c2\3\2\2\2\u00bf\u00c1\5\32\16\2\u00c0\u00bf\3\2\2")
        buf.write("\2\u00c1\u00c4\3\2\2\2\u00c2\u00c0\3\2\2\2\u00c2\u00c3")
        buf.write("\3\2\2\2\u00c3\u00c8\3\2\2\2\u00c4\u00c2\3\2\2\2\u00c5")
        buf.write("\u00c7\5 \21\2\u00c6\u00c5\3\2\2\2\u00c7\u00ca\3\2\2\2")
        buf.write("\u00c8\u00c6\3\2\2\2\u00c8\u00c9\3\2\2\2\u00c9\u00ce\3")
        buf.write("\2\2\2\u00ca\u00c8\3\2\2\2\u00cb\u00cd\58\35\2\u00cc\u00cb")
        buf.write("\3\2\2\2\u00cd\u00d0\3\2\2\2\u00ce\u00cc\3\2\2\2\u00ce")
        buf.write("\u00cf\3\2\2\2\u00cf\u00d1\3\2\2\2\u00d0\u00ce\3\2\2\2")
        buf.write("\u00d1\u00d2\7\6\2\2\u00d2\5\3\2\2\2\u00d3\u00d4\7I\2")
        buf.write("\2\u00d4\7\3\2\2\2\u00d5\u00d6\7\3\2\2\u00d6\u00d7\7\7")
        buf.write("\2\2\u00d7\u00d8\5\n\6\2\u00d8\u00d9\7\6\2\2\u00d9\t\3")
        buf.write("\2\2\2\u00da\u00dc\7G\2\2\u00db\u00da\3\2\2\2\u00dc\u00dd")
        buf.write("\3\2\2\2\u00dd\u00db\3\2\2\2\u00dd\u00de\3\2\2\2\u00de")
        buf.write("\13\3\2\2\2\u00df\u00e0\7\3\2\2\u00e0\u00e1\7\b\2\2\u00e1")
        buf.write("\u00e2\5\16\b\2\u00e2\u00e3\7\6\2\2\u00e3\r\3\2\2\2\u00e4")
        buf.write("\u00e6\7I\2\2\u00e5\u00e4\3\2\2\2\u00e6\u00e9\3\2\2\2")
        buf.write("\u00e7\u00e5\3\2\2\2\u00e7\u00e8\3\2\2\2\u00e8\u00f0\3")
        buf.write("\2\2\2\u00e9\u00e7\3\2\2\2\u00ea\u00eb\5\20\t\2\u00eb")
        buf.write("\u00ec\7\t\2\2\u00ec\u00ed\5\u0086D\2\u00ed\u00ee\5\16")
        buf.write("\b\2\u00ee\u00f0\3\2\2\2\u00ef\u00e7\3\2\2\2\u00ef\u00ea")
        buf.write("\3\2\2\2\u00f0\17\3\2\2\2\u00f1\u00f3\7I\2\2\u00f2\u00f1")
        buf.write("\3\2\2\2\u00f3\u00f4\3\2\2\2\u00f4\u00f2\3\2\2\2\u00f4")
        buf.write("\u00f5\3\2\2\2\u00f5\21\3\2\2\2\u00f6\u00f7\7\3\2\2\u00f7")
        buf.write("\u00f8\7\n\2\2\u00f8\u00f9\5|?\2\u00f9\u00fa\7\6\2\2\u00fa")
        buf.write("\23\3\2\2\2\u00fb\u00fc\7\3\2\2\u00fc\u00fe\7\13\2\2\u00fd")
        buf.write("\u00ff\5\26\f\2\u00fe\u00fd\3\2\2\2\u00ff\u0100\3\2\2")
        buf.write("\2\u0100\u00fe\3\2\2\2\u0100\u0101\3\2\2\2\u0101\u0102")
        buf.write("\3\2\2\2\u0102\u0103\7\6\2\2\u0103\25\3\2\2\2\u0104\u0105")
        buf.write("\7\3\2\2\u0105\u0106\5v<\2\u0106\u0107\5z>\2\u0107\u0108")
        buf.write("\7\6\2\2\u0108\27\3\2\2\2\u0109\u010a\7\3\2\2\u010a\u0111")
        buf.write("\7\f\2\2\u010b\u010f\5\26\f\2\u010c\u010d\7\t\2\2\u010d")
        buf.write("\u0110\7\r\2\2\u010e\u0110\5\u0086D\2\u010f\u010c\3\2")
        buf.write("\2\2\u010f\u010e\3\2\2\2\u010f\u0110\3\2\2\2\u0110\u0112")
        buf.write("\3\2\2\2\u0111\u010b\3\2\2\2\u0112\u0113\3\2\2\2\u0113")
        buf.write("\u0111\3\2\2\2\u0113\u0114\3\2\2\2\u0114\u0115\3\2\2\2")
        buf.write("\u0115\u0116\7\6\2\2\u0116\31\3\2\2\2\u0117\u0118\7\3")
        buf.write("\2\2\u0118\u0119\7\16\2\2\u0119\u011a\5\34\17\2\u011a")
        buf.write("\33\3\2\2\2\u011b\u011c\5\36\20\2\u011c\u011d\7\17\2\2")
        buf.write("\u011d\u011e\7\3\2\2\u011e\u011f\5z>\2\u011f\u0122\7\6")
        buf.write("\2\2\u0120\u0121\7\20\2\2\u0121\u0123\5:\36\2\u0122\u0120")
        buf.write("\3\2\2\2\u0122\u0123\3\2\2\2\u0123\u0126\3\2\2\2\u0124")
        buf.write("\u0125\7\21\2\2\u0125\u0127\5Z.\2\u0126\u0124\3\2\2\2")
        buf.write("\u0126\u0127\3\2\2\2\u0127\u0128\3\2\2\2\u0128\u0129\7")
        buf.write("\6\2\2\u0129\35\3\2\2\2\u012a\u012b\7I\2\2\u012b\37\3")
        buf.write("\2\2\2\u012c\u012d\7\3\2\2\u012d\u012e\7\22\2\2\u012e")
        buf.write("\u012f\5$\23\2\u012f\u0130\7\17\2\2\u0130\u0131\7\3\2")
        buf.write("\2\u0131\u0132\5z>\2\u0132\u0133\7\6\2\2\u0133\u0134\7")
        buf.write("\16\2\2\u0134\u0135\7\3\2\2\u0135\u0139\5\36\20\2\u0136")
        buf.write("\u0138\5\u0088E\2\u0137\u0136\3\2\2\2\u0138\u013b\3\2")
        buf.write("\2\2\u0139\u0137\3\2\2\2\u0139\u013a\3\2\2\2\u013a\u013c")
        buf.write("\3\2\2\2\u013b\u0139\3\2\2\2\u013c\u013f\7\6\2\2\u013d")
        buf.write("\u013e\7\20\2\2\u013e\u0140\5:\36\2\u013f\u013d\3\2\2")
        buf.write("\2\u013f\u0140\3\2\2\2\u0140\u0143\3\2\2\2\u0141\u0142")
        buf.write("\7\21\2\2\u0142\u0144\5Z.\2\u0143\u0141\3\2\2\2\u0143")
        buf.write("\u0144\3\2\2\2\u0144\u0145\3\2\2\2\u0145\u0146\5\"\22")
        buf.write("\2\u0146!\3\2\2\2\u0147\u0148\t\2\2\2\u0148\u014a\5&\24")
        buf.write("\2\u0149\u0147\3\2\2\2\u0149\u014a\3\2\2\2\u014a\u014d")
        buf.write("\3\2\2\2\u014b\u014c\t\3\2\2\u014c\u014e\5,\27\2\u014d")
        buf.write("\u014b\3\2\2\2\u014d\u014e\3\2\2\2\u014e\u0151\3\2\2\2")
        buf.write("\u014f\u0150\7\31\2\2\u0150\u0152\5\60\31\2\u0151\u014f")
        buf.write("\3\2\2\2\u0151\u0152\3\2\2\2\u0152\u0155\3\2\2\2\u0153")
        buf.write("\u0154\t\4\2\2\u0154\u0156\5\64\33\2\u0155\u0153\3\2\2")
        buf.write("\2\u0155\u0156\3\2\2\2\u0156\u0157\3\2\2\2\u0157\u0158")
        buf.write("\7\6\2\2\u0158#\3\2\2\2\u0159\u015a\7I\2\2\u015a%\3\2")
        buf.write("\2\2\u015b\u015c\7\3\2\2\u015c\u0168\7\6\2\2\u015d\u0168")
        buf.write("\5(\25\2\u015e\u015f\7\3\2\2\u015f\u0161\7\34\2\2\u0160")
        buf.write("\u0162\5(\25\2\u0161\u0160\3\2\2\2\u0162\u0163\3\2\2\2")
        buf.write("\u0163\u0161\3\2\2\2\u0163\u0164\3\2\2\2\u0164\u0165\3")
        buf.write("\2\2\2\u0165\u0166\7\6\2\2\u0166\u0168\3\2\2\2\u0167\u015b")
        buf.write("\3\2\2\2\u0167\u015d\3\2\2\2\u0167\u015e\3\2\2\2\u0168")
        buf.write("\'\3\2\2\2\u0169\u016a\7\3\2\2\u016a\u016e\5\36\20\2\u016b")
        buf.write("\u016d\5\u0088E\2\u016c\u016b\3\2\2\2\u016d\u0170\3\2")
        buf.write("\2\2\u016e\u016c\3\2\2\2\u016e\u016f\3\2\2\2\u016f\u0171")
        buf.write("\3\2\2\2\u0170\u016e\3\2\2\2\u0171\u0172\7\6\2\2\u0172")
        buf.write("\u0181\3\2\2\2\u0173\u0174\7\3\2\2\u0174\u0175\5*\26\2")
        buf.write("\u0175\u0176\7\3\2\2\u0176\u017a\5\36\20\2\u0177\u0179")
        buf.write("\5\u0088E\2\u0178\u0177\3\2\2\2\u0179\u017c\3\2\2\2\u017a")
        buf.write("\u0178\3\2\2\2\u017a\u017b\3\2\2\2\u017b\u017d\3\2\2\2")
        buf.write("\u017c\u017a\3\2\2\2\u017d\u017e\7\6\2\2\u017e\u017f\7")
        buf.write("\6\2\2\u017f\u0181\3\2\2\2\u0180\u0169\3\2\2\2\u0180\u0173")
        buf.write("\3\2\2\2\u0181)\3\2\2\2\u0182\u0183\7I\2\2\u0183+\3\2")
        buf.write("\2\2\u0184\u0185\7\3\2\2\u0185\u0191\7\6\2\2\u0186\u0191")
        buf.write("\5.\30\2\u0187\u0188\7\3\2\2\u0188\u018a\7\34\2\2\u0189")
        buf.write("\u018b\5.\30\2\u018a\u0189\3\2\2\2\u018b\u018c\3\2\2\2")
        buf.write("\u018c\u018a\3\2\2\2\u018c\u018d\3\2\2\2\u018d\u018e\3")
        buf.write("\2\2\2\u018e\u018f\7\6\2\2\u018f\u0191\3\2\2\2\u0190\u0184")
        buf.write("\3\2\2\2\u0190\u0186\3\2\2\2\u0190\u0187\3\2\2\2\u0191")
        buf.write("-\3\2\2\2\u0192\u0193\7\3\2\2\u0193\u0194\7\35\2\2\u0194")
        buf.write("\u0195\5*\26\2\u0195\u0196\5*\26\2\u0196\u0197\7\6\2\2")
        buf.write("\u0197\u019f\3\2\2\2\u0198\u0199\7\3\2\2\u0199\u019a\5")
        buf.write("*\26\2\u019a\u019b\7\35\2\2\u019b\u019c\5*\26\2\u019c")
        buf.write("\u019d\7\6\2\2\u019d\u019f\3\2\2\2\u019e\u0192\3\2\2\2")
        buf.write("\u019e\u0198\3\2\2\2\u019f/\3\2\2\2\u01a0\u01a1\7\3\2")
        buf.write("\2\u01a1\u01ad\7\6\2\2\u01a2\u01ad\5\62\32\2\u01a3\u01a4")
        buf.write("\7\3\2\2\u01a4\u01a6\7\34\2\2\u01a5\u01a7\5\62\32\2\u01a6")
        buf.write("\u01a5\3\2\2\2\u01a7\u01a8\3\2\2\2\u01a8\u01a6\3\2\2\2")
        buf.write("\u01a8\u01a9\3\2\2\2\u01a9\u01aa\3\2\2\2\u01aa\u01ab\7")
        buf.write("\6\2\2\u01ab\u01ad\3\2\2\2\u01ac\u01a0\3\2\2\2\u01ac\u01a2")
        buf.write("\3\2\2\2\u01ac\u01a3\3\2\2\2\u01ad\61\3\2\2\2\u01ae\u01af")
        buf.write("\7\3\2\2\u01af\u01cb\7\6\2\2\u01b0\u01b1\7\3\2\2\u01b1")
        buf.write("\u01b2\7\36\2\2\u01b2\u01b3\5x=\2\u01b3\u01b4\5\u0088")
        buf.write("E\2\u01b4\u01b5\5\u0088E\2\u01b5\u01b6\7\6\2\2\u01b6\u01b7")
        buf.write("\7\6\2\2\u01b7\u01cb\3\2\2\2\u01b8\u01b9\5x=\2\u01b9\u01ba")
        buf.write("\5\u0088E\2\u01ba\u01bb\5\u0088E\2\u01bb\u01bc\7\6\2\2")
        buf.write("\u01bc\u01cb\3\2\2\2\u01bd\u01be\7\3\2\2\u01be\u01bf\t")
        buf.write("\5\2\2\u01bf\u01c0\5\u0080A\2\u01c0\u01c1\7\6\2\2\u01c1")
        buf.write("\u01cb\3\2\2\2\u01c2\u01c3\7\3\2\2\u01c3\u01c4\7\36\2")
        buf.write("\2\u01c4\u01c5\7\3\2\2\u01c5\u01c6\t\5\2\2\u01c6\u01c7")
        buf.write("\5\u0080A\2\u01c7\u01c8\7\6\2\2\u01c8\u01c9\7\6\2\2\u01c9")
        buf.write("\u01cb\3\2\2\2\u01ca\u01ae\3\2\2\2\u01ca\u01b0\3\2\2\2")
        buf.write("\u01ca\u01b8\3\2\2\2\u01ca\u01bd\3\2\2\2\u01ca\u01c2\3")
        buf.write("\2\2\2\u01cb\63\3\2\2\2\u01cc\u01cd\7\3\2\2\u01cd\u01d9")
        buf.write("\7\6\2\2\u01ce\u01d9\5\66\34\2\u01cf\u01d0\7\3\2\2\u01d0")
        buf.write("\u01d2\7\34\2\2\u01d1\u01d3\5\66\34\2\u01d2\u01d1\3\2")
        buf.write("\2\2\u01d3\u01d4\3\2\2\2\u01d4\u01d2\3\2\2\2\u01d4\u01d5")
        buf.write("\3\2\2\2\u01d5\u01d6\3\2\2\2\u01d6\u01d7\7\6\2\2\u01d7")
        buf.write("\u01d9\3\2\2\2\u01d8\u01cc\3\2\2\2\u01d8\u01ce\3\2\2\2")
        buf.write("\u01d8\u01cf\3\2\2\2\u01d9\65\3\2\2\2\u01da\u01db\7\3")
        buf.write("\2\2\u01db\u01dc\5*\26\2\u01dc\u01dd\5d\63\2\u01dd\u01de")
        buf.write("\5*\26\2\u01de\u01df\7\6\2\2\u01df\67\3\2\2\2\u01e0\u01e1")
        buf.write("\7\3\2\2\u01e1\u01e2\7#\2\2\u01e2\u01e3\5\34\17\2\u01e3")
        buf.write("9\3\2\2\2\u01e4\u01f5\5<\37\2\u01e5\u01f5\5t;\2\u01e6")
        buf.write("\u01f5\5B\"\2\u01e7\u01f5\5D#\2\u01e8\u01f5\5> \2\u01e9")
        buf.write("\u01f5\5@!\2\u01ea\u01f5\5F$\2\u01eb\u01f5\5H%\2\u01ec")
        buf.write("\u01f5\5J&\2\u01ed\u01f5\5L\'\2\u01ee\u01f5\5N(\2\u01ef")
        buf.write("\u01f5\5P)\2\u01f0\u01f5\5R*\2\u01f1\u01f5\5T+\2\u01f2")
        buf.write("\u01f5\5V,\2\u01f3\u01f5\5X-\2\u01f4\u01e4\3\2\2\2\u01f4")
        buf.write("\u01e5\3\2\2\2\u01f4\u01e6\3\2\2\2\u01f4\u01e7\3\2\2\2")
        buf.write("\u01f4\u01e8\3\2\2\2\u01f4\u01e9\3\2\2\2\u01f4\u01ea\3")
        buf.write("\2\2\2\u01f4\u01eb\3\2\2\2\u01f4\u01ec\3\2\2\2\u01f4\u01ed")
        buf.write("\3\2\2\2\u01f4\u01ee\3\2\2\2\u01f4\u01ef\3\2\2\2\u01f4")
        buf.write("\u01f0\3\2\2\2\u01f4\u01f1\3\2\2\2\u01f4\u01f2\3\2\2\2")
        buf.write("\u01f4\u01f3\3\2\2\2\u01f5;\3\2\2\2\u01f6\u01f7\7\3\2")
        buf.write("\2\u01f7\u01f8\7\6\2\2\u01f8=\3\2\2\2\u01f9\u01fa\7\3")
        buf.write("\2\2\u01fa\u01fc\7\34\2\2\u01fb\u01fd\5:\36\2\u01fc\u01fb")
        buf.write("\3\2\2\2\u01fd\u01fe\3\2\2\2\u01fe\u01fc\3\2\2\2\u01fe")
        buf.write("\u01ff\3\2\2\2\u01ff\u0200\3\2\2\2\u0200\u0201\7\6\2\2")
        buf.write("\u0201?\3\2\2\2\u0202\u0203\7\3\2\2\u0203\u0205\7$\2\2")
        buf.write("\u0204\u0206\5:\36\2\u0205\u0204\3\2\2\2\u0206\u0207\3")
        buf.write("\2\2\2\u0207\u0205\3\2\2\2\u0207\u0208\3\2\2\2\u0208\u0209")
        buf.write("\3\2\2\2\u0209\u020a\7\6\2\2\u020aA\3\2\2\2\u020b\u020c")
        buf.write("\7\3\2\2\u020c\u020d\7\36\2\2\u020d\u020e\5:\36\2\u020e")
        buf.write("\u020f\7\6\2\2\u020fC\3\2\2\2\u0210\u0211\7\3\2\2\u0211")
        buf.write("\u0212\7%\2\2\u0212\u0213\5:\36\2\u0213\u0214\5:\36\2")
        buf.write("\u0214\u0215\7\6\2\2\u0215E\3\2\2\2\u0216\u0217\7\3\2")
        buf.write("\2\u0217\u0218\7&\2\2\u0218\u0219\7\3\2\2\u0219\u021a")
        buf.write("\5z>\2\u021a\u021b\7\6\2\2\u021b\u021c\5:\36\2\u021c\u021d")
        buf.write("\7\6\2\2\u021dG\3\2\2\2\u021e\u021f\7\3\2\2\u021f\u0220")
        buf.write("\7\'\2\2\u0220\u0221\7\3\2\2\u0221\u0222\5z>\2\u0222\u0223")
        buf.write("\7\6\2\2\u0223\u0224\5:\36\2\u0224\u0225\7\6\2\2\u0225")
        buf.write("I\3\2\2\2\u0226\u0227\5x=\2\u0227\u0228\5\u0088E\2\u0228")
        buf.write("\u0229\5\u0088E\2\u0229\u022a\7\6\2\2\u022aK\3\2\2\2\u022b")
        buf.write("\u022c\7\3\2\2\u022c\u022d\7(\2\2\u022d\u022e\5:\36\2")
        buf.write("\u022e\u022f\7\6\2\2\u022fM\3\2\2\2\u0230\u0231\7\3\2")
        buf.write("\2\u0231\u0232\7)\2\2\u0232\u0233\5:\36\2\u0233\u0234")
        buf.write("\7\6\2\2\u0234O\3\2\2\2\u0235\u0236\7\3\2\2\u0236\u0237")
        buf.write("\7*\2\2\u0237\u0238\5:\36\2\u0238\u0239\7\6\2\2\u0239")
        buf.write("Q\3\2\2\2\u023a\u023b\7\3\2\2\u023b\u023c\7+\2\2\u023c")
        buf.write("\u023d\5:\36\2\u023d\u023e\7\6\2\2\u023eS\3\2\2\2\u023f")
        buf.write("\u0240\7\3\2\2\u0240\u0241\7,\2\2\u0241\u0242\5:\36\2")
        buf.write("\u0242\u0243\5:\36\2\u0243\u0244\7\6\2\2\u0244U\3\2\2")
        buf.write("\2\u0245\u0246\7\3\2\2\u0246\u0247\7-\2\2\u0247\u0248")
        buf.write("\5:\36\2\u0248\u0249\5:\36\2\u0249\u024a\7\6\2\2\u024a")
        buf.write("W\3\2\2\2\u024b\u024c\7\3\2\2\u024c\u024d\7.\2\2\u024d")
        buf.write("\u024e\7I\2\2\u024e\u024f\5:\36\2\u024f\u0250\7\6\2\2")
        buf.write("\u0250Y\3\2\2\2\u0251\u0258\5\\/\2\u0252\u0258\5^\60\2")
        buf.write("\u0253\u0258\5`\61\2\u0254\u0258\5b\62\2\u0255\u0258\5")
        buf.write("d\63\2\u0256\u0258\5h\65\2\u0257\u0251\3\2\2\2\u0257\u0252")
        buf.write("\3\2\2\2\u0257\u0253\3\2\2\2\u0257\u0254\3\2\2\2\u0257")
        buf.write("\u0255\3\2\2\2\u0257\u0256\3\2\2\2\u0258[\3\2\2\2\u0259")
        buf.write("\u025a\7\3\2\2\u025a\u025b\7\6\2\2\u025b]\3\2\2\2\u025c")
        buf.write("\u025d\7\3\2\2\u025d\u025f\7\34\2\2\u025e\u0260\5Z.\2")
        buf.write("\u025f\u025e\3\2\2\2\u0260\u0261\3\2\2\2\u0261\u025f\3")
        buf.write("\2\2\2\u0261\u0262\3\2\2\2\u0262\u0263\3\2\2\2\u0263\u0264")
        buf.write("\7\6\2\2\u0264_\3\2\2\2\u0265\u0266\7\3\2\2\u0266\u0267")
        buf.write("\7\'\2\2\u0267\u0268\7\3\2\2\u0268\u0269\5z>\2\u0269\u026a")
        buf.write("\7\6\2\2\u026a\u026b\5Z.\2\u026b\u026c\7\6\2\2\u026ca")
        buf.write("\3\2\2\2\u026d\u026e\7\3\2\2\u026e\u026f\7/\2\2\u026f")
        buf.write("\u0270\5:\36\2\u0270\u0271\5Z.\2\u0271\u0272\7\6\2\2\u0272")
        buf.write("c\3\2\2\2\u0273\u0276\5f\64\2\u0274\u0276\5t;\2\u0275")
        buf.write("\u0273\3\2\2\2\u0275\u0274\3\2\2\2\u0276e\3\2\2\2\u0277")
        buf.write("\u0278\7\3\2\2\u0278\u0279\7\36\2\2\u0279\u027a\5t;\2")
        buf.write("\u027a\u027b\7\6\2\2\u027bg\3\2\2\2\u027c\u027d\7\3\2")
        buf.write("\2\u027d\u027e\5j\66\2\u027e\u027f\5l\67\2\u027f\u0280")
        buf.write("\5n8\2\u0280\u0281\7\6\2\2\u0281i\3\2\2\2\u0282\u0283")
        buf.write("\t\6\2\2\u0283k\3\2\2\2\u0284\u0290\5\u008eH\2\u0285\u0286")
        buf.write("\7\3\2\2\u0286\u028a\5\u008eH\2\u0287\u0289\5\u008aF\2")
        buf.write("\u0288\u0287\3\2\2\2\u0289\u028c\3\2\2\2\u028a\u0288\3")
        buf.write("\2\2\2\u028a\u028b\3\2\2\2\u028b\u028d\3\2\2\2\u028c\u028a")
        buf.write("\3\2\2\2\u028d\u028e\7\6\2\2\u028e\u0290\3\2\2\2\u028f")
        buf.write("\u0284\3\2\2\2\u028f\u0285\3\2\2\2\u0290m\3\2\2\2\u0291")
        buf.write("\u02a9\7L\2\2\u0292\u0293\7\3\2\2\u0293\u0294\5p9\2\u0294")
        buf.write("\u0295\5n8\2\u0295\u0296\5n8\2\u0296\u0297\7\6\2\2\u0297")
        buf.write("\u02a9\3\2\2\2\u0298\u0299\7\3\2\2\u0299\u029a\5r:\2\u029a")
        buf.write("\u029c\5n8\2\u029b\u029d\5n8\2\u029c\u029b\3\2\2\2\u029d")
        buf.write("\u029e\3\2\2\2\u029e\u029c\3\2\2\2\u029e\u029f\3\2\2\2")
        buf.write("\u029f\u02a0\3\2\2\2\u02a0\u02a1\7\6\2\2\u02a1\u02a9\3")
        buf.write("\2\2\2\u02a2\u02a3\7\3\2\2\u02a3\u02a4\7\t\2\2\u02a4\u02a5")
        buf.write("\5n8\2\u02a5\u02a6\7\6\2\2\u02a6\u02a9\3\2\2\2\u02a7\u02a9")
        buf.write("\5l\67\2\u02a8\u0291\3\2\2\2\u02a8\u0292\3\2\2\2\u02a8")
        buf.write("\u0298\3\2\2\2\u02a8\u02a2\3\2\2\2\u02a8\u02a7\3\2\2\2")
        buf.write("\u02a9o\3\2\2\2\u02aa\u02ae\5r:\2\u02ab\u02ae\7\t\2\2")
        buf.write("\u02ac\u02ae\7\65\2\2\u02ad\u02aa\3\2\2\2\u02ad\u02ab")
        buf.write("\3\2\2\2\u02ad\u02ac\3\2\2\2\u02aeq\3\2\2\2\u02af\u02b0")
        buf.write("\t\7\2\2\u02b0s\3\2\2\2\u02b1\u02b2\7\3\2\2\u02b2\u02b6")
        buf.write("\5v<\2\u02b3\u02b5\5\u0088E\2\u02b4\u02b3\3\2\2\2\u02b5")
        buf.write("\u02b8\3\2\2\2\u02b6\u02b4\3\2\2\2\u02b6\u02b7\3\2\2\2")
        buf.write("\u02b7\u02b9\3\2\2\2\u02b8\u02b6\3\2\2\2\u02b9\u02ba\7")
        buf.write("\6\2\2\u02bau\3\2\2\2\u02bb\u02bc\7I\2\2\u02bcw\3\2\2")
        buf.write("\2\u02bd\u02be\7\3\2\2\u02be\u02c1\78\2\2\u02bf\u02c1")
        buf.write("\79\2\2\u02c0\u02bd\3\2\2\2\u02c0\u02bf\3\2\2\2\u02c1")
        buf.write("y\3\2\2\2\u02c2\u02c4\5~@\2\u02c3\u02c2\3\2\2\2\u02c4")
        buf.write("\u02c7\3\2\2\2\u02c5\u02c3\3\2\2\2\u02c5\u02c6\3\2\2\2")
        buf.write("\u02c6{\3\2\2\2\u02c7\u02c5\3\2\2\2\u02c8\u02ca\5\u0082")
        buf.write("B\2\u02c9\u02c8\3\2\2\2\u02ca\u02cd\3\2\2\2\u02cb\u02c9")
        buf.write("\3\2\2\2\u02cb\u02cc\3\2\2\2\u02cc}\3\2\2\2\u02cd\u02cb")
        buf.write("\3\2\2\2\u02ce\u02d0\7H\2\2\u02cf\u02ce\3\2\2\2\u02d0")
        buf.write("\u02d1\3\2\2\2\u02d1\u02cf\3\2\2\2\u02d1\u02d2\3\2\2\2")
        buf.write("\u02d2\u02d3\3\2\2\2\u02d3\u02d4\7\t\2\2\u02d4\u02d5\5")
        buf.write("\u0086D\2\u02d5\177\3\2\2\2\u02d6\u02d7\7H\2\2\u02d7\u02d8")
        buf.write("\7\t\2\2\u02d8\u02d9\5\u0086D\2\u02d9\u0081\3\2\2\2\u02da")
        buf.write("\u02dc\5\u0084C\2\u02db\u02da\3\2\2\2\u02dc\u02dd\3\2")
        buf.write("\2\2\u02dd\u02db\3\2\2\2\u02dd\u02de\3\2\2\2\u02de\u02df")
        buf.write("\3\2\2\2\u02df\u02e0\7\t\2\2\u02e0\u02e1\5\u0086D\2\u02e1")
        buf.write("\u0083\3\2\2\2\u02e2\u02e3\7I\2\2\u02e3\u0085\3\2\2\2")
        buf.write("\u02e4\u02ef\7I\2\2\u02e5\u02e6\7\3\2\2\u02e6\u02e8\7")
        buf.write(":\2\2\u02e7\u02e9\5\u0086D\2\u02e8\u02e7\3\2\2\2\u02e9")
        buf.write("\u02ea\3\2\2\2\u02ea\u02e8\3\2\2\2\u02ea\u02eb\3\2\2\2")
        buf.write("\u02eb\u02ec\3\2\2\2\u02ec\u02ed\7\6\2\2\u02ed\u02ef\3")
        buf.write("\2\2\2\u02ee\u02e4\3\2\2\2\u02ee\u02e5\3\2\2\2\u02ef\u0087")
        buf.write("\3\2\2\2\u02f0\u02f1\t\b\2\2\u02f1\u0089\3\2\2\2\u02f2")
        buf.write("\u02f6\7I\2\2\u02f3\u02f6\7H\2\2\u02f4\u02f6\5\u008cG")
        buf.write("\2\u02f5\u02f2\3\2\2\2\u02f5\u02f3\3\2\2\2\u02f5\u02f4")
        buf.write("\3\2\2\2\u02f6\u008b\3\2\2\2\u02f7\u02f8\7\3\2\2\u02f8")
        buf.write("\u02fc\5\u008eH\2\u02f9\u02fb\5\u008aF\2\u02fa\u02f9\3")
        buf.write("\2\2\2\u02fb\u02fe\3\2\2\2\u02fc\u02fa\3\2\2\2\u02fc\u02fd")
        buf.write("\3\2\2\2\u02fd\u02ff\3\2\2\2\u02fe\u02fc\3\2\2\2\u02ff")
        buf.write("\u0300\7\6\2\2\u0300\u008d\3\2\2\2\u0301\u0302\7I\2\2")
        buf.write("\u0302\u008f\3\2\2\2\u0303\u0304\7\3\2\2\u0304\u0305\7")
        buf.write("\4\2\2\u0305\u0306\7\3\2\2\u0306\u0307\7;\2\2\u0307\u0308")
        buf.write("\7I\2\2\u0308\u0309\7\6\2\2\u0309\u030a\7\3\2\2\u030a")
        buf.write("\u030b\7<\2\2\u030b\u030c\7I\2\2\u030c\u030e\7\6\2\2\u030d")
        buf.write("\u030f\5\b\5\2\u030e\u030d\3\2\2\2\u030e\u030f\3\2\2\2")
        buf.write("\u030f\u0311\3\2\2\2\u0310\u0312\5\u0092J\2\u0311\u0310")
        buf.write("\3\2\2\2\u0311\u0312\3\2\2\2\u0312\u0314\3\2\2\2\u0313")
        buf.write("\u0315\5\u009cO\2\u0314\u0313\3\2\2\2\u0314\u0315\3\2")
        buf.write("\2\2\u0315\u0316\3\2\2\2\u0316\u0318\5\u0094K\2\u0317")
        buf.write("\u0319\5\u009aN\2\u0318\u0317\3\2\2\2\u0318\u0319\3\2")
        buf.write("\2\2\u0319\u031b\3\2\2\2\u031a\u031c\5\u00a4S\2\u031b")
        buf.write("\u031a\3\2\2\2\u031b\u031c\3\2\2\2\u031c\u031e\3\2\2\2")
        buf.write("\u031d\u031f\5\u009eP\2\u031e\u031d\3\2\2\2\u031e\u031f")
        buf.write("\3\2\2\2\u031f\u0320\3\2\2\2\u0320\u0321\7\6\2\2\u0321")
        buf.write("\u0091\3\2\2\2\u0322\u0323\7\3\2\2\u0323\u0324\7=\2\2")
        buf.write("\u0324\u0325\5|?\2\u0325\u0326\7\6\2\2\u0326\u0093\3\2")
        buf.write("\2\2\u0327\u0328\7\3\2\2\u0328\u032c\7>\2\2\u0329\u032b")
        buf.write("\5\u0096L\2\u032a\u0329\3\2\2\2\u032b\u032e\3\2\2\2\u032c")
        buf.write("\u032a\3\2\2\2\u032c\u032d\3\2\2\2\u032d\u032f\3\2\2\2")
        buf.write("\u032e\u032c\3\2\2\2\u032f\u0330\7\6\2\2\u0330\u0095\3")
        buf.write("\2\2\2\u0331\u0334\5d\63\2\u0332\u0334\5\u0098M\2\u0333")
        buf.write("\u0331\3\2\2\2\u0333\u0332\3\2\2\2\u0334\u0097\3\2\2\2")
        buf.write("\u0335\u0336\5x=\2\u0336\u0337\5l\67\2\u0337\u0338\7L")
        buf.write("\2\2\u0338\u0339\7\6\2\2\u0339\u0099\3\2\2\2\u033a\u033b")
        buf.write("\7\3\2\2\u033b\u033c\7?\2\2\u033c\u033d\5:\36\2\u033d")
        buf.write("\u033e\7\6\2\2\u033e\u009b\3\2\2\2\u033f\u0340\7\3\2\2")
        buf.write("\u0340\u0346\t\t\2\2\u0341\u0342\7\17\2\2\u0342\u0343")
        buf.write("\7\3\2\2\u0343\u0344\5z>\2\u0344\u0345\7\6\2\2\u0345\u0347")
        buf.write("\3\2\2\2\u0346\u0341\3\2\2\2\u0346\u0347\3\2\2\2\u0347")
        buf.write("\u0348\3\2\2\2\u0348\u0349\5\"\22\2\u0349\u009d\3\2\2")
        buf.write("\2\u034a\u034b\7\3\2\2\u034b\u034c\7B\2\2\u034c\u034d")
        buf.write("\5\u00a0Q\2\u034d\u034e\5\u00a2R\2\u034e\u034f\7\6\2\2")
        buf.write("\u034f\u009f\3\2\2\2\u0350\u0351\t\n\2\2\u0351\u00a1\3")
        buf.write("\2\2\2\u0352\u0353\7\3\2\2\u0353\u0354\5p9\2\u0354\u0355")
        buf.write("\5\u00a2R\2\u0355\u0356\5\u00a2R\2\u0356\u0357\7\6\2\2")
        buf.write("\u0357\u0378\3\2\2\2\u0358\u0359\7\3\2\2\u0359\u035a\5")
        buf.write("r:\2\u035a\u035c\5\u00a2R\2\u035b\u035d\5\u00a2R\2\u035c")
        buf.write("\u035b\3\2\2\2\u035d\u035e\3\2\2\2\u035e\u035c\3\2\2\2")
        buf.write("\u035e\u035f\3\2\2\2\u035f\u0360\3\2\2\2\u0360\u0361\7")
        buf.write("\6\2\2\u0361\u0378\3\2\2\2\u0362\u0363\7\3\2\2\u0363\u0366")
        buf.write("\7\t\2\2\u0364\u0366\7E\2\2\u0365\u0362\3\2\2\2\u0365")
        buf.write("\u0364\3\2\2\2\u0366\u0367\3\2\2\2\u0367\u0368\5\u00a2")
        buf.write("R\2\u0368\u0369\7\6\2\2\u0369\u0378\3\2\2\2\u036a\u0378")
        buf.write("\7L\2\2\u036b\u036c\7\3\2\2\u036c\u0370\5\u008eH\2\u036d")
        buf.write("\u036f\7I\2\2\u036e\u036d\3\2\2\2\u036f\u0372\3\2\2\2")
        buf.write("\u0370\u036e\3\2\2\2\u0370\u0371\3\2\2\2\u0371\u0373\3")
        buf.write("\2\2\2\u0372\u0370\3\2\2\2\u0373\u0374\7\6\2\2\u0374\u0378")
        buf.write("\3\2\2\2\u0375\u0378\7F\2\2\u0376\u0378\5\u008eH\2\u0377")
        buf.write("\u0352\3\2\2\2\u0377\u0358\3\2\2\2\u0377\u0365\3\2\2\2")
        buf.write("\u0377\u036a\3\2\2\2\u0377\u036b\3\2\2\2\u0377\u0375\3")
        buf.write("\2\2\2\u0377\u0376\3\2\2\2\u0378\u00a3\3\2\2\2\u0379\u037a")
        buf.write("\7\3\2\2\u037a\u037b\7\31\2\2\u037b\u037c\5:\36\2\u037c")
        buf.write("\u037d\7\6\2\2\u037d\u00a5\3\2\2\2J\u00a8\u00b1\u00b4")
        buf.write("\u00b7\u00ba\u00bd\u00c2\u00c8\u00ce\u00dd\u00e7\u00ef")
        buf.write("\u00f4\u0100\u010f\u0113\u0122\u0126\u0139\u013f\u0143")
        buf.write("\u0149\u014d\u0151\u0155\u0163\u0167\u016e\u017a\u0180")
        buf.write("\u018c\u0190\u019e\u01a8\u01ac\u01ca\u01d4\u01d8\u01f4")
        buf.write("\u01fe\u0207\u0257\u0261\u0275\u028a\u028f\u029e\u02a8")
        buf.write("\u02ad\u02b6\u02c0\u02c5\u02cb\u02d1\u02dd\u02ea\u02ee")
        buf.write("\u02f5\u02fc\u030e\u0311\u0314\u0318\u031b\u031e\u032c")
        buf.write("\u0333\u0346\u035e\u0365\u0370\u0377")
        return buf.getvalue()


class antlrHDDLParser ( Parser ):

    grammarFileName = "antlrHDDL.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'('", "'define'", "'domain'", "')'", 
                     "':requirements'", "':types'", "'-'", "':constants'", 
                     "':predicates'", "':functions'", "'number'", "':task'", 
                     "':parameters'", "':precondition'", "':effect'", "':method'", 
                     "':subtasks'", "':tasks'", "':ordered-subtasks'", "':ordered-tasks'", 
                     "':ordering'", "':order'", "':constraints'", "':causal-links'", 
                     "':causallinks'", "'and'", "'<'", "'not'", "'type'", 
                     "'typeof'", "'sort'", "'sortof'", "':action'", "'or'", 
                     "'imply'", "'exists'", "'forall'", "'at end'", "'always'", 
                     "'sometime'", "'at-most-once'", "'sometime-after'", 
                     "'sometime-before'", "'preference'", "'when'", "'assign'", 
                     "'scale-down'", "'scale-up'", "'increase'", "'decrease'", 
                     "'/'", "'+'", "'*'", "'='", "'(='", "'either'", "'problem'", 
                     "':domain'", "':objects'", "':init'", "':goal'", "':htn'", 
                     "':htnti'", "':metric'", "'minimize'", "'maximize'", 
                     "'(-'", "'total-time'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "REQUIRE_NAME", "VAR_NAME", "NAME", "COMMENT", 
                      "WS", "NUMBER" ]

    RULE_hddl_file = 0
    RULE_domain = 1
    RULE_domain_symbol = 2
    RULE_require_def = 3
    RULE_require_defs = 4
    RULE_type_def = 5
    RULE_type_def_list = 6
    RULE_new_types = 7
    RULE_const_def = 8
    RULE_predicates_def = 9
    RULE_atomic_formula_skeleton = 10
    RULE_funtions_def = 11
    RULE_comp_task_def = 12
    RULE_task_def = 13
    RULE_task_symbol = 14
    RULE_method_def = 15
    RULE_tasknetwork_def = 16
    RULE_method_symbol = 17
    RULE_subtask_defs = 18
    RULE_subtask_def = 19
    RULE_subtask_id = 20
    RULE_ordering_defs = 21
    RULE_ordering_def = 22
    RULE_constraint_defs = 23
    RULE_constraint_def = 24
    RULE_causallink_defs = 25
    RULE_causallink_def = 26
    RULE_action_def = 27
    RULE_gd = 28
    RULE_gd_empty = 29
    RULE_gd_conjuction = 30
    RULE_gd_disjuction = 31
    RULE_gd_negation = 32
    RULE_gd_implication = 33
    RULE_gd_existential = 34
    RULE_gd_universal = 35
    RULE_gd_equality_constraint = 36
    RULE_gd_ltl_at_end = 37
    RULE_gd_ltl_always = 38
    RULE_gd_ltl_sometime = 39
    RULE_gd_ltl_at_most_once = 40
    RULE_gd_ltl_sometime_after = 41
    RULE_gd_ltl_sometime_before = 42
    RULE_gd_preference = 43
    RULE_effect = 44
    RULE_eff_empty = 45
    RULE_eff_conjunction = 46
    RULE_eff_universal = 47
    RULE_eff_conditional = 48
    RULE_literal = 49
    RULE_neg_atomic_formula = 50
    RULE_p_effect = 51
    RULE_assign_op = 52
    RULE_f_head = 53
    RULE_f_exp = 54
    RULE_bin_op = 55
    RULE_multi_op = 56
    RULE_atomic_formula = 57
    RULE_predicate = 58
    RULE_equallity = 59
    RULE_typed_var_list = 60
    RULE_typed_obj_list = 61
    RULE_typed_vars = 62
    RULE_typed_var = 63
    RULE_typed_objs = 64
    RULE_new_consts = 65
    RULE_var_type = 66
    RULE_var_or_const = 67
    RULE_term = 68
    RULE_functionterm = 69
    RULE_func_symbol = 70
    RULE_problem = 71
    RULE_p_object_declaration = 72
    RULE_p_init = 73
    RULE_init_el = 74
    RULE_num_init = 75
    RULE_p_goal = 76
    RULE_p_htn = 77
    RULE_metric_spec = 78
    RULE_optimization = 79
    RULE_ground_f_exp = 80
    RULE_p_constraint = 81

    ruleNames =  [ "hddl_file", "domain", "domain_symbol", "require_def", 
                   "require_defs", "type_def", "type_def_list", "new_types", 
                   "const_def", "predicates_def", "atomic_formula_skeleton", 
                   "funtions_def", "comp_task_def", "task_def", "task_symbol", 
                   "method_def", "tasknetwork_def", "method_symbol", "subtask_defs", 
                   "subtask_def", "subtask_id", "ordering_defs", "ordering_def", 
                   "constraint_defs", "constraint_def", "causallink_defs", 
                   "causallink_def", "action_def", "gd", "gd_empty", "gd_conjuction", 
                   "gd_disjuction", "gd_negation", "gd_implication", "gd_existential", 
                   "gd_universal", "gd_equality_constraint", "gd_ltl_at_end", 
                   "gd_ltl_always", "gd_ltl_sometime", "gd_ltl_at_most_once", 
                   "gd_ltl_sometime_after", "gd_ltl_sometime_before", "gd_preference", 
                   "effect", "eff_empty", "eff_conjunction", "eff_universal", 
                   "eff_conditional", "literal", "neg_atomic_formula", "p_effect", 
                   "assign_op", "f_head", "f_exp", "bin_op", "multi_op", 
                   "atomic_formula", "predicate", "equallity", "typed_var_list", 
                   "typed_obj_list", "typed_vars", "typed_var", "typed_objs", 
                   "new_consts", "var_type", "var_or_const", "term", "functionterm", 
                   "func_symbol", "problem", "p_object_declaration", "p_init", 
                   "init_el", "num_init", "p_goal", "p_htn", "metric_spec", 
                   "optimization", "ground_f_exp", "p_constraint" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    T__8=9
    T__9=10
    T__10=11
    T__11=12
    T__12=13
    T__13=14
    T__14=15
    T__15=16
    T__16=17
    T__17=18
    T__18=19
    T__19=20
    T__20=21
    T__21=22
    T__22=23
    T__23=24
    T__24=25
    T__25=26
    T__26=27
    T__27=28
    T__28=29
    T__29=30
    T__30=31
    T__31=32
    T__32=33
    T__33=34
    T__34=35
    T__35=36
    T__36=37
    T__37=38
    T__38=39
    T__39=40
    T__40=41
    T__41=42
    T__42=43
    T__43=44
    T__44=45
    T__45=46
    T__46=47
    T__47=48
    T__48=49
    T__49=50
    T__50=51
    T__51=52
    T__52=53
    T__53=54
    T__54=55
    T__55=56
    T__56=57
    T__57=58
    T__58=59
    T__59=60
    T__60=61
    T__61=62
    T__62=63
    T__63=64
    T__64=65
    T__65=66
    T__66=67
    T__67=68
    REQUIRE_NAME=69
    VAR_NAME=70
    NAME=71
    COMMENT=72
    WS=73
    NUMBER=74

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.8")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class Hddl_fileContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def domain(self):
            return self.getTypedRuleContext(antlrHDDLParser.DomainContext,0)


        def problem(self):
            return self.getTypedRuleContext(antlrHDDLParser.ProblemContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_hddl_file

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterHddl_file" ):
                listener.enterHddl_file(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitHddl_file" ):
                listener.exitHddl_file(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitHddl_file" ):
                return visitor.visitHddl_file(self)
            else:
                return visitor.visitChildren(self)




    def hddl_file(self):

        localctx = antlrHDDLParser.Hddl_fileContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_hddl_file)
        try:
            self.state = 166
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,0,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 164
                self.domain()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 165
                self.problem()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DomainContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def domain_symbol(self):
            return self.getTypedRuleContext(antlrHDDLParser.Domain_symbolContext,0)


        def require_def(self):
            return self.getTypedRuleContext(antlrHDDLParser.Require_defContext,0)


        def type_def(self):
            return self.getTypedRuleContext(antlrHDDLParser.Type_defContext,0)


        def const_def(self):
            return self.getTypedRuleContext(antlrHDDLParser.Const_defContext,0)


        def predicates_def(self):
            return self.getTypedRuleContext(antlrHDDLParser.Predicates_defContext,0)


        def funtions_def(self):
            return self.getTypedRuleContext(antlrHDDLParser.Funtions_defContext,0)


        def comp_task_def(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.Comp_task_defContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.Comp_task_defContext,i)


        def method_def(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.Method_defContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.Method_defContext,i)


        def action_def(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.Action_defContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.Action_defContext,i)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_domain

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDomain" ):
                listener.enterDomain(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDomain" ):
                listener.exitDomain(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDomain" ):
                return visitor.visitDomain(self)
            else:
                return visitor.visitChildren(self)




    def domain(self):

        localctx = antlrHDDLParser.DomainContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_domain)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 168
            self.match(antlrHDDLParser.T__0)
            self.state = 169
            self.match(antlrHDDLParser.T__1)
            self.state = 170
            self.match(antlrHDDLParser.T__0)
            self.state = 171
            self.match(antlrHDDLParser.T__2)
            self.state = 172
            self.domain_symbol()
            self.state = 173
            self.match(antlrHDDLParser.T__3)
            self.state = 175
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,1,self._ctx)
            if la_ == 1:
                self.state = 174
                self.require_def()


            self.state = 178
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,2,self._ctx)
            if la_ == 1:
                self.state = 177
                self.type_def()


            self.state = 181
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,3,self._ctx)
            if la_ == 1:
                self.state = 180
                self.const_def()


            self.state = 184
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,4,self._ctx)
            if la_ == 1:
                self.state = 183
                self.predicates_def()


            self.state = 187
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,5,self._ctx)
            if la_ == 1:
                self.state = 186
                self.funtions_def()


            self.state = 192
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,6,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 189
                    self.comp_task_def() 
                self.state = 194
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,6,self._ctx)

            self.state = 198
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,7,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 195
                    self.method_def() 
                self.state = 200
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,7,self._ctx)

            self.state = 204
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==antlrHDDLParser.T__0:
                self.state = 201
                self.action_def()
                self.state = 206
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 207
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Domain_symbolContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NAME(self):
            return self.getToken(antlrHDDLParser.NAME, 0)

        def getRuleIndex(self):
            return antlrHDDLParser.RULE_domain_symbol

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDomain_symbol" ):
                listener.enterDomain_symbol(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDomain_symbol" ):
                listener.exitDomain_symbol(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDomain_symbol" ):
                return visitor.visitDomain_symbol(self)
            else:
                return visitor.visitChildren(self)




    def domain_symbol(self):

        localctx = antlrHDDLParser.Domain_symbolContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_domain_symbol)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 209
            self.match(antlrHDDLParser.NAME)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Require_defContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def require_defs(self):
            return self.getTypedRuleContext(antlrHDDLParser.Require_defsContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_require_def

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRequire_def" ):
                listener.enterRequire_def(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRequire_def" ):
                listener.exitRequire_def(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRequire_def" ):
                return visitor.visitRequire_def(self)
            else:
                return visitor.visitChildren(self)




    def require_def(self):

        localctx = antlrHDDLParser.Require_defContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_require_def)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 211
            self.match(antlrHDDLParser.T__0)
            self.state = 212
            self.match(antlrHDDLParser.T__4)
            self.state = 213
            self.require_defs()
            self.state = 214
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Require_defsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def REQUIRE_NAME(self, i:int=None):
            if i is None:
                return self.getTokens(antlrHDDLParser.REQUIRE_NAME)
            else:
                return self.getToken(antlrHDDLParser.REQUIRE_NAME, i)

        def getRuleIndex(self):
            return antlrHDDLParser.RULE_require_defs

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRequire_defs" ):
                listener.enterRequire_defs(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRequire_defs" ):
                listener.exitRequire_defs(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRequire_defs" ):
                return visitor.visitRequire_defs(self)
            else:
                return visitor.visitChildren(self)




    def require_defs(self):

        localctx = antlrHDDLParser.Require_defsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_require_defs)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 217 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 216
                self.match(antlrHDDLParser.REQUIRE_NAME)
                self.state = 219 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==antlrHDDLParser.REQUIRE_NAME):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Type_defContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_def_list(self):
            return self.getTypedRuleContext(antlrHDDLParser.Type_def_listContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_type_def

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterType_def" ):
                listener.enterType_def(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitType_def" ):
                listener.exitType_def(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitType_def" ):
                return visitor.visitType_def(self)
            else:
                return visitor.visitChildren(self)




    def type_def(self):

        localctx = antlrHDDLParser.Type_defContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_type_def)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 221
            self.match(antlrHDDLParser.T__0)
            self.state = 222
            self.match(antlrHDDLParser.T__5)
            self.state = 223
            self.type_def_list()
            self.state = 224
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Type_def_listContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NAME(self, i:int=None):
            if i is None:
                return self.getTokens(antlrHDDLParser.NAME)
            else:
                return self.getToken(antlrHDDLParser.NAME, i)

        def new_types(self):
            return self.getTypedRuleContext(antlrHDDLParser.New_typesContext,0)


        def var_type(self):
            return self.getTypedRuleContext(antlrHDDLParser.Var_typeContext,0)


        def type_def_list(self):
            return self.getTypedRuleContext(antlrHDDLParser.Type_def_listContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_type_def_list

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterType_def_list" ):
                listener.enterType_def_list(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitType_def_list" ):
                listener.exitType_def_list(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitType_def_list" ):
                return visitor.visitType_def_list(self)
            else:
                return visitor.visitChildren(self)




    def type_def_list(self):

        localctx = antlrHDDLParser.Type_def_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_type_def_list)
        self._la = 0 # Token type
        try:
            self.state = 237
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,11,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 229
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==antlrHDDLParser.NAME:
                    self.state = 226
                    self.match(antlrHDDLParser.NAME)
                    self.state = 231
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 232
                self.new_types()
                self.state = 233
                self.match(antlrHDDLParser.T__6)
                self.state = 234
                self.var_type()
                self.state = 235
                self.type_def_list()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class New_typesContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NAME(self, i:int=None):
            if i is None:
                return self.getTokens(antlrHDDLParser.NAME)
            else:
                return self.getToken(antlrHDDLParser.NAME, i)

        def getRuleIndex(self):
            return antlrHDDLParser.RULE_new_types

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNew_types" ):
                listener.enterNew_types(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNew_types" ):
                listener.exitNew_types(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNew_types" ):
                return visitor.visitNew_types(self)
            else:
                return visitor.visitChildren(self)




    def new_types(self):

        localctx = antlrHDDLParser.New_typesContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_new_types)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 240 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 239
                self.match(antlrHDDLParser.NAME)
                self.state = 242 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==antlrHDDLParser.NAME):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Const_defContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def typed_obj_list(self):
            return self.getTypedRuleContext(antlrHDDLParser.Typed_obj_listContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_const_def

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterConst_def" ):
                listener.enterConst_def(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitConst_def" ):
                listener.exitConst_def(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitConst_def" ):
                return visitor.visitConst_def(self)
            else:
                return visitor.visitChildren(self)




    def const_def(self):

        localctx = antlrHDDLParser.Const_defContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_const_def)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 244
            self.match(antlrHDDLParser.T__0)
            self.state = 245
            self.match(antlrHDDLParser.T__7)
            self.state = 246
            self.typed_obj_list()
            self.state = 247
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Predicates_defContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def atomic_formula_skeleton(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.Atomic_formula_skeletonContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.Atomic_formula_skeletonContext,i)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_predicates_def

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPredicates_def" ):
                listener.enterPredicates_def(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPredicates_def" ):
                listener.exitPredicates_def(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPredicates_def" ):
                return visitor.visitPredicates_def(self)
            else:
                return visitor.visitChildren(self)




    def predicates_def(self):

        localctx = antlrHDDLParser.Predicates_defContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_predicates_def)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 249
            self.match(antlrHDDLParser.T__0)
            self.state = 250
            self.match(antlrHDDLParser.T__8)
            self.state = 252 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 251
                self.atomic_formula_skeleton()
                self.state = 254 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==antlrHDDLParser.T__0):
                    break

            self.state = 256
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Atomic_formula_skeletonContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def predicate(self):
            return self.getTypedRuleContext(antlrHDDLParser.PredicateContext,0)


        def typed_var_list(self):
            return self.getTypedRuleContext(antlrHDDLParser.Typed_var_listContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_atomic_formula_skeleton

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAtomic_formula_skeleton" ):
                listener.enterAtomic_formula_skeleton(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAtomic_formula_skeleton" ):
                listener.exitAtomic_formula_skeleton(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAtomic_formula_skeleton" ):
                return visitor.visitAtomic_formula_skeleton(self)
            else:
                return visitor.visitChildren(self)




    def atomic_formula_skeleton(self):

        localctx = antlrHDDLParser.Atomic_formula_skeletonContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_atomic_formula_skeleton)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 258
            self.match(antlrHDDLParser.T__0)
            self.state = 259
            self.predicate()
            self.state = 260
            self.typed_var_list()
            self.state = 261
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Funtions_defContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def atomic_formula_skeleton(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.Atomic_formula_skeletonContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.Atomic_formula_skeletonContext,i)


        def var_type(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.Var_typeContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.Var_typeContext,i)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_funtions_def

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFuntions_def" ):
                listener.enterFuntions_def(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFuntions_def" ):
                listener.exitFuntions_def(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFuntions_def" ):
                return visitor.visitFuntions_def(self)
            else:
                return visitor.visitChildren(self)




    def funtions_def(self):

        localctx = antlrHDDLParser.Funtions_defContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_funtions_def)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 263
            self.match(antlrHDDLParser.T__0)
            self.state = 264
            self.match(antlrHDDLParser.T__9)
            self.state = 271 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 265
                self.atomic_formula_skeleton()
                self.state = 269
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,14,self._ctx)
                if la_ == 1:
                    self.state = 266
                    self.match(antlrHDDLParser.T__6)
                    self.state = 267
                    self.match(antlrHDDLParser.T__10)

                elif la_ == 2:
                    self.state = 268
                    self.var_type()


                self.state = 273 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==antlrHDDLParser.T__0):
                    break

            self.state = 275
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Comp_task_defContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def task_def(self):
            return self.getTypedRuleContext(antlrHDDLParser.Task_defContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_comp_task_def

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterComp_task_def" ):
                listener.enterComp_task_def(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitComp_task_def" ):
                listener.exitComp_task_def(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitComp_task_def" ):
                return visitor.visitComp_task_def(self)
            else:
                return visitor.visitChildren(self)




    def comp_task_def(self):

        localctx = antlrHDDLParser.Comp_task_defContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_comp_task_def)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 277
            self.match(antlrHDDLParser.T__0)
            self.state = 278
            self.match(antlrHDDLParser.T__11)
            self.state = 279
            self.task_def()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Task_defContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def task_symbol(self):
            return self.getTypedRuleContext(antlrHDDLParser.Task_symbolContext,0)


        def typed_var_list(self):
            return self.getTypedRuleContext(antlrHDDLParser.Typed_var_listContext,0)


        def gd(self):
            return self.getTypedRuleContext(antlrHDDLParser.GdContext,0)


        def effect(self):
            return self.getTypedRuleContext(antlrHDDLParser.EffectContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_task_def

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTask_def" ):
                listener.enterTask_def(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTask_def" ):
                listener.exitTask_def(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTask_def" ):
                return visitor.visitTask_def(self)
            else:
                return visitor.visitChildren(self)




    def task_def(self):

        localctx = antlrHDDLParser.Task_defContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_task_def)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 281
            self.task_symbol()
            self.state = 282
            self.match(antlrHDDLParser.T__12)
            self.state = 283
            self.match(antlrHDDLParser.T__0)
            self.state = 284
            self.typed_var_list()
            self.state = 285
            self.match(antlrHDDLParser.T__3)
            self.state = 288
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==antlrHDDLParser.T__13:
                self.state = 286
                self.match(antlrHDDLParser.T__13)
                self.state = 287
                self.gd()


            self.state = 292
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==antlrHDDLParser.T__14:
                self.state = 290
                self.match(antlrHDDLParser.T__14)
                self.state = 291
                self.effect()


            self.state = 294
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Task_symbolContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NAME(self):
            return self.getToken(antlrHDDLParser.NAME, 0)

        def getRuleIndex(self):
            return antlrHDDLParser.RULE_task_symbol

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTask_symbol" ):
                listener.enterTask_symbol(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTask_symbol" ):
                listener.exitTask_symbol(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTask_symbol" ):
                return visitor.visitTask_symbol(self)
            else:
                return visitor.visitChildren(self)




    def task_symbol(self):

        localctx = antlrHDDLParser.Task_symbolContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_task_symbol)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 296
            self.match(antlrHDDLParser.NAME)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Method_defContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def method_symbol(self):
            return self.getTypedRuleContext(antlrHDDLParser.Method_symbolContext,0)


        def typed_var_list(self):
            return self.getTypedRuleContext(antlrHDDLParser.Typed_var_listContext,0)


        def task_symbol(self):
            return self.getTypedRuleContext(antlrHDDLParser.Task_symbolContext,0)


        def tasknetwork_def(self):
            return self.getTypedRuleContext(antlrHDDLParser.Tasknetwork_defContext,0)


        def var_or_const(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.Var_or_constContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.Var_or_constContext,i)


        def gd(self):
            return self.getTypedRuleContext(antlrHDDLParser.GdContext,0)


        def effect(self):
            return self.getTypedRuleContext(antlrHDDLParser.EffectContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_method_def

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMethod_def" ):
                listener.enterMethod_def(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMethod_def" ):
                listener.exitMethod_def(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMethod_def" ):
                return visitor.visitMethod_def(self)
            else:
                return visitor.visitChildren(self)




    def method_def(self):

        localctx = antlrHDDLParser.Method_defContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_method_def)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 298
            self.match(antlrHDDLParser.T__0)
            self.state = 299
            self.match(antlrHDDLParser.T__15)
            self.state = 300
            self.method_symbol()
            self.state = 301
            self.match(antlrHDDLParser.T__12)
            self.state = 302
            self.match(antlrHDDLParser.T__0)
            self.state = 303
            self.typed_var_list()
            self.state = 304
            self.match(antlrHDDLParser.T__3)
            self.state = 305
            self.match(antlrHDDLParser.T__11)
            self.state = 306
            self.match(antlrHDDLParser.T__0)
            self.state = 307
            self.task_symbol()
            self.state = 311
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==antlrHDDLParser.VAR_NAME or _la==antlrHDDLParser.NAME:
                self.state = 308
                self.var_or_const()
                self.state = 313
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 314
            self.match(antlrHDDLParser.T__3)
            self.state = 317
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==antlrHDDLParser.T__13:
                self.state = 315
                self.match(antlrHDDLParser.T__13)
                self.state = 316
                self.gd()


            self.state = 321
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==antlrHDDLParser.T__14:
                self.state = 319
                self.match(antlrHDDLParser.T__14)
                self.state = 320
                self.effect()


            self.state = 323
            self.tasknetwork_def()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Tasknetwork_defContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def subtask_defs(self):
            return self.getTypedRuleContext(antlrHDDLParser.Subtask_defsContext,0)


        def ordering_defs(self):
            return self.getTypedRuleContext(antlrHDDLParser.Ordering_defsContext,0)


        def constraint_defs(self):
            return self.getTypedRuleContext(antlrHDDLParser.Constraint_defsContext,0)


        def causallink_defs(self):
            return self.getTypedRuleContext(antlrHDDLParser.Causallink_defsContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_tasknetwork_def

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTasknetwork_def" ):
                listener.enterTasknetwork_def(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTasknetwork_def" ):
                listener.exitTasknetwork_def(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTasknetwork_def" ):
                return visitor.visitTasknetwork_def(self)
            else:
                return visitor.visitChildren(self)




    def tasknetwork_def(self):

        localctx = antlrHDDLParser.Tasknetwork_defContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_tasknetwork_def)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 327
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << antlrHDDLParser.T__16) | (1 << antlrHDDLParser.T__17) | (1 << antlrHDDLParser.T__18) | (1 << antlrHDDLParser.T__19))) != 0):
                self.state = 325
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << antlrHDDLParser.T__16) | (1 << antlrHDDLParser.T__17) | (1 << antlrHDDLParser.T__18) | (1 << antlrHDDLParser.T__19))) != 0)):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 326
                self.subtask_defs()


            self.state = 331
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==antlrHDDLParser.T__20 or _la==antlrHDDLParser.T__21:
                self.state = 329
                _la = self._input.LA(1)
                if not(_la==antlrHDDLParser.T__20 or _la==antlrHDDLParser.T__21):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 330
                self.ordering_defs()


            self.state = 335
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==antlrHDDLParser.T__22:
                self.state = 333
                self.match(antlrHDDLParser.T__22)
                self.state = 334
                self.constraint_defs()


            self.state = 339
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==antlrHDDLParser.T__23 or _la==antlrHDDLParser.T__24:
                self.state = 337
                _la = self._input.LA(1)
                if not(_la==antlrHDDLParser.T__23 or _la==antlrHDDLParser.T__24):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 338
                self.causallink_defs()


            self.state = 341
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Method_symbolContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NAME(self):
            return self.getToken(antlrHDDLParser.NAME, 0)

        def getRuleIndex(self):
            return antlrHDDLParser.RULE_method_symbol

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMethod_symbol" ):
                listener.enterMethod_symbol(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMethod_symbol" ):
                listener.exitMethod_symbol(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMethod_symbol" ):
                return visitor.visitMethod_symbol(self)
            else:
                return visitor.visitChildren(self)




    def method_symbol(self):

        localctx = antlrHDDLParser.Method_symbolContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_method_symbol)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 343
            self.match(antlrHDDLParser.NAME)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Subtask_defsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def subtask_def(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.Subtask_defContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.Subtask_defContext,i)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_subtask_defs

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSubtask_defs" ):
                listener.enterSubtask_defs(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSubtask_defs" ):
                listener.exitSubtask_defs(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSubtask_defs" ):
                return visitor.visitSubtask_defs(self)
            else:
                return visitor.visitChildren(self)




    def subtask_defs(self):

        localctx = antlrHDDLParser.Subtask_defsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_subtask_defs)
        self._la = 0 # Token type
        try:
            self.state = 357
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,26,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 345
                self.match(antlrHDDLParser.T__0)
                self.state = 346
                self.match(antlrHDDLParser.T__3)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 347
                self.subtask_def()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 348
                self.match(antlrHDDLParser.T__0)
                self.state = 349
                self.match(antlrHDDLParser.T__25)
                self.state = 351 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 350
                    self.subtask_def()
                    self.state = 353 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not (_la==antlrHDDLParser.T__0):
                        break

                self.state = 355
                self.match(antlrHDDLParser.T__3)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Subtask_defContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def task_symbol(self):
            return self.getTypedRuleContext(antlrHDDLParser.Task_symbolContext,0)


        def subtask_id(self):
            return self.getTypedRuleContext(antlrHDDLParser.Subtask_idContext,0)


        def var_or_const(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.Var_or_constContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.Var_or_constContext,i)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_subtask_def

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSubtask_def" ):
                listener.enterSubtask_def(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSubtask_def" ):
                listener.exitSubtask_def(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSubtask_def" ):
                return visitor.visitSubtask_def(self)
            else:
                return visitor.visitChildren(self)




    def subtask_def(self):

        localctx = antlrHDDLParser.Subtask_defContext(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_subtask_def)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 382
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,29,self._ctx)
            if la_ == 1:
                self.state = 359
                self.match(antlrHDDLParser.T__0)
                self.state = 360
                self.task_symbol()
                self.state = 364
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==antlrHDDLParser.VAR_NAME or _la==antlrHDDLParser.NAME:
                    self.state = 361
                    self.var_or_const()
                    self.state = 366
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 367
                self.match(antlrHDDLParser.T__3)
                pass

            elif la_ == 2:
                self.state = 369
                self.match(antlrHDDLParser.T__0)
                self.state = 370
                self.subtask_id()
                self.state = 371
                self.match(antlrHDDLParser.T__0)
                self.state = 372
                self.task_symbol()
                self.state = 376
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==antlrHDDLParser.VAR_NAME or _la==antlrHDDLParser.NAME:
                    self.state = 373
                    self.var_or_const()
                    self.state = 378
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 379
                self.match(antlrHDDLParser.T__3)
                self.state = 380
                self.match(antlrHDDLParser.T__3)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Subtask_idContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NAME(self):
            return self.getToken(antlrHDDLParser.NAME, 0)

        def getRuleIndex(self):
            return antlrHDDLParser.RULE_subtask_id

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSubtask_id" ):
                listener.enterSubtask_id(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSubtask_id" ):
                listener.exitSubtask_id(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSubtask_id" ):
                return visitor.visitSubtask_id(self)
            else:
                return visitor.visitChildren(self)




    def subtask_id(self):

        localctx = antlrHDDLParser.Subtask_idContext(self, self._ctx, self.state)
        self.enterRule(localctx, 40, self.RULE_subtask_id)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 384
            self.match(antlrHDDLParser.NAME)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Ordering_defsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ordering_def(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.Ordering_defContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.Ordering_defContext,i)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_ordering_defs

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterOrdering_defs" ):
                listener.enterOrdering_defs(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitOrdering_defs" ):
                listener.exitOrdering_defs(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitOrdering_defs" ):
                return visitor.visitOrdering_defs(self)
            else:
                return visitor.visitChildren(self)




    def ordering_defs(self):

        localctx = antlrHDDLParser.Ordering_defsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_ordering_defs)
        self._la = 0 # Token type
        try:
            self.state = 398
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,31,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 386
                self.match(antlrHDDLParser.T__0)
                self.state = 387
                self.match(antlrHDDLParser.T__3)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 388
                self.ordering_def()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 389
                self.match(antlrHDDLParser.T__0)
                self.state = 390
                self.match(antlrHDDLParser.T__25)
                self.state = 392 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 391
                    self.ordering_def()
                    self.state = 394 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not (_la==antlrHDDLParser.T__0):
                        break

                self.state = 396
                self.match(antlrHDDLParser.T__3)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Ordering_defContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def subtask_id(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.Subtask_idContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.Subtask_idContext,i)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_ordering_def

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterOrdering_def" ):
                listener.enterOrdering_def(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitOrdering_def" ):
                listener.exitOrdering_def(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitOrdering_def" ):
                return visitor.visitOrdering_def(self)
            else:
                return visitor.visitChildren(self)




    def ordering_def(self):

        localctx = antlrHDDLParser.Ordering_defContext(self, self._ctx, self.state)
        self.enterRule(localctx, 44, self.RULE_ordering_def)
        try:
            self.state = 412
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,32,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 400
                self.match(antlrHDDLParser.T__0)
                self.state = 401
                self.match(antlrHDDLParser.T__26)
                self.state = 402
                self.subtask_id()
                self.state = 403
                self.subtask_id()
                self.state = 404
                self.match(antlrHDDLParser.T__3)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 406
                self.match(antlrHDDLParser.T__0)
                self.state = 407
                self.subtask_id()
                self.state = 408
                self.match(antlrHDDLParser.T__26)
                self.state = 409
                self.subtask_id()
                self.state = 410
                self.match(antlrHDDLParser.T__3)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Constraint_defsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def constraint_def(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.Constraint_defContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.Constraint_defContext,i)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_constraint_defs

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterConstraint_defs" ):
                listener.enterConstraint_defs(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitConstraint_defs" ):
                listener.exitConstraint_defs(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitConstraint_defs" ):
                return visitor.visitConstraint_defs(self)
            else:
                return visitor.visitChildren(self)




    def constraint_defs(self):

        localctx = antlrHDDLParser.Constraint_defsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 46, self.RULE_constraint_defs)
        self._la = 0 # Token type
        try:
            self.state = 426
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,34,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 414
                self.match(antlrHDDLParser.T__0)
                self.state = 415
                self.match(antlrHDDLParser.T__3)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 416
                self.constraint_def()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 417
                self.match(antlrHDDLParser.T__0)
                self.state = 418
                self.match(antlrHDDLParser.T__25)
                self.state = 420 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 419
                    self.constraint_def()
                    self.state = 422 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not (_la==antlrHDDLParser.T__0 or _la==antlrHDDLParser.T__54):
                        break

                self.state = 424
                self.match(antlrHDDLParser.T__3)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Constraint_defContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def equallity(self):
            return self.getTypedRuleContext(antlrHDDLParser.EquallityContext,0)


        def var_or_const(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.Var_or_constContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.Var_or_constContext,i)


        def typed_var(self):
            return self.getTypedRuleContext(antlrHDDLParser.Typed_varContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_constraint_def

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterConstraint_def" ):
                listener.enterConstraint_def(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitConstraint_def" ):
                listener.exitConstraint_def(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitConstraint_def" ):
                return visitor.visitConstraint_def(self)
            else:
                return visitor.visitChildren(self)




    def constraint_def(self):

        localctx = antlrHDDLParser.Constraint_defContext(self, self._ctx, self.state)
        self.enterRule(localctx, 48, self.RULE_constraint_def)
        self._la = 0 # Token type
        try:
            self.state = 456
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,35,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 428
                self.match(antlrHDDLParser.T__0)
                self.state = 429
                self.match(antlrHDDLParser.T__3)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 430
                self.match(antlrHDDLParser.T__0)
                self.state = 431
                self.match(antlrHDDLParser.T__27)
                self.state = 432
                self.equallity()
                self.state = 433
                self.var_or_const()
                self.state = 434
                self.var_or_const()
                self.state = 435
                self.match(antlrHDDLParser.T__3)
                self.state = 436
                self.match(antlrHDDLParser.T__3)
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 438
                self.equallity()
                self.state = 439
                self.var_or_const()
                self.state = 440
                self.var_or_const()
                self.state = 441
                self.match(antlrHDDLParser.T__3)
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 443
                self.match(antlrHDDLParser.T__0)
                self.state = 444
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << antlrHDDLParser.T__28) | (1 << antlrHDDLParser.T__29) | (1 << antlrHDDLParser.T__30) | (1 << antlrHDDLParser.T__31))) != 0)):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 445
                self.typed_var()
                self.state = 446
                self.match(antlrHDDLParser.T__3)
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 448
                self.match(antlrHDDLParser.T__0)
                self.state = 449
                self.match(antlrHDDLParser.T__27)
                self.state = 450
                self.match(antlrHDDLParser.T__0)
                self.state = 451
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << antlrHDDLParser.T__28) | (1 << antlrHDDLParser.T__29) | (1 << antlrHDDLParser.T__30) | (1 << antlrHDDLParser.T__31))) != 0)):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 452
                self.typed_var()
                self.state = 453
                self.match(antlrHDDLParser.T__3)
                self.state = 454
                self.match(antlrHDDLParser.T__3)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Causallink_defsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def causallink_def(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.Causallink_defContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.Causallink_defContext,i)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_causallink_defs

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCausallink_defs" ):
                listener.enterCausallink_defs(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCausallink_defs" ):
                listener.exitCausallink_defs(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCausallink_defs" ):
                return visitor.visitCausallink_defs(self)
            else:
                return visitor.visitChildren(self)




    def causallink_defs(self):

        localctx = antlrHDDLParser.Causallink_defsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 50, self.RULE_causallink_defs)
        self._la = 0 # Token type
        try:
            self.state = 470
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,37,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 458
                self.match(antlrHDDLParser.T__0)
                self.state = 459
                self.match(antlrHDDLParser.T__3)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 460
                self.causallink_def()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 461
                self.match(antlrHDDLParser.T__0)
                self.state = 462
                self.match(antlrHDDLParser.T__25)
                self.state = 464 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 463
                    self.causallink_def()
                    self.state = 466 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not (_la==antlrHDDLParser.T__0):
                        break

                self.state = 468
                self.match(antlrHDDLParser.T__3)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Causallink_defContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def subtask_id(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.Subtask_idContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.Subtask_idContext,i)


        def literal(self):
            return self.getTypedRuleContext(antlrHDDLParser.LiteralContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_causallink_def

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCausallink_def" ):
                listener.enterCausallink_def(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCausallink_def" ):
                listener.exitCausallink_def(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCausallink_def" ):
                return visitor.visitCausallink_def(self)
            else:
                return visitor.visitChildren(self)




    def causallink_def(self):

        localctx = antlrHDDLParser.Causallink_defContext(self, self._ctx, self.state)
        self.enterRule(localctx, 52, self.RULE_causallink_def)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 472
            self.match(antlrHDDLParser.T__0)
            self.state = 473
            self.subtask_id()
            self.state = 474
            self.literal()
            self.state = 475
            self.subtask_id()
            self.state = 476
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Action_defContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def task_def(self):
            return self.getTypedRuleContext(antlrHDDLParser.Task_defContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_action_def

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAction_def" ):
                listener.enterAction_def(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAction_def" ):
                listener.exitAction_def(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAction_def" ):
                return visitor.visitAction_def(self)
            else:
                return visitor.visitChildren(self)




    def action_def(self):

        localctx = antlrHDDLParser.Action_defContext(self, self._ctx, self.state)
        self.enterRule(localctx, 54, self.RULE_action_def)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 478
            self.match(antlrHDDLParser.T__0)
            self.state = 479
            self.match(antlrHDDLParser.T__32)
            self.state = 480
            self.task_def()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class GdContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def gd_empty(self):
            return self.getTypedRuleContext(antlrHDDLParser.Gd_emptyContext,0)


        def atomic_formula(self):
            return self.getTypedRuleContext(antlrHDDLParser.Atomic_formulaContext,0)


        def gd_negation(self):
            return self.getTypedRuleContext(antlrHDDLParser.Gd_negationContext,0)


        def gd_implication(self):
            return self.getTypedRuleContext(antlrHDDLParser.Gd_implicationContext,0)


        def gd_conjuction(self):
            return self.getTypedRuleContext(antlrHDDLParser.Gd_conjuctionContext,0)


        def gd_disjuction(self):
            return self.getTypedRuleContext(antlrHDDLParser.Gd_disjuctionContext,0)


        def gd_existential(self):
            return self.getTypedRuleContext(antlrHDDLParser.Gd_existentialContext,0)


        def gd_universal(self):
            return self.getTypedRuleContext(antlrHDDLParser.Gd_universalContext,0)


        def gd_equality_constraint(self):
            return self.getTypedRuleContext(antlrHDDLParser.Gd_equality_constraintContext,0)


        def gd_ltl_at_end(self):
            return self.getTypedRuleContext(antlrHDDLParser.Gd_ltl_at_endContext,0)


        def gd_ltl_always(self):
            return self.getTypedRuleContext(antlrHDDLParser.Gd_ltl_alwaysContext,0)


        def gd_ltl_sometime(self):
            return self.getTypedRuleContext(antlrHDDLParser.Gd_ltl_sometimeContext,0)


        def gd_ltl_at_most_once(self):
            return self.getTypedRuleContext(antlrHDDLParser.Gd_ltl_at_most_onceContext,0)


        def gd_ltl_sometime_after(self):
            return self.getTypedRuleContext(antlrHDDLParser.Gd_ltl_sometime_afterContext,0)


        def gd_ltl_sometime_before(self):
            return self.getTypedRuleContext(antlrHDDLParser.Gd_ltl_sometime_beforeContext,0)


        def gd_preference(self):
            return self.getTypedRuleContext(antlrHDDLParser.Gd_preferenceContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_gd

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGd" ):
                listener.enterGd(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGd" ):
                listener.exitGd(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGd" ):
                return visitor.visitGd(self)
            else:
                return visitor.visitChildren(self)




    def gd(self):

        localctx = antlrHDDLParser.GdContext(self, self._ctx, self.state)
        self.enterRule(localctx, 56, self.RULE_gd)
        try:
            self.state = 498
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,38,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 482
                self.gd_empty()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 483
                self.atomic_formula()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 484
                self.gd_negation()
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 485
                self.gd_implication()
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 486
                self.gd_conjuction()
                pass

            elif la_ == 6:
                self.enterOuterAlt(localctx, 6)
                self.state = 487
                self.gd_disjuction()
                pass

            elif la_ == 7:
                self.enterOuterAlt(localctx, 7)
                self.state = 488
                self.gd_existential()
                pass

            elif la_ == 8:
                self.enterOuterAlt(localctx, 8)
                self.state = 489
                self.gd_universal()
                pass

            elif la_ == 9:
                self.enterOuterAlt(localctx, 9)
                self.state = 490
                self.gd_equality_constraint()
                pass

            elif la_ == 10:
                self.enterOuterAlt(localctx, 10)
                self.state = 491
                self.gd_ltl_at_end()
                pass

            elif la_ == 11:
                self.enterOuterAlt(localctx, 11)
                self.state = 492
                self.gd_ltl_always()
                pass

            elif la_ == 12:
                self.enterOuterAlt(localctx, 12)
                self.state = 493
                self.gd_ltl_sometime()
                pass

            elif la_ == 13:
                self.enterOuterAlt(localctx, 13)
                self.state = 494
                self.gd_ltl_at_most_once()
                pass

            elif la_ == 14:
                self.enterOuterAlt(localctx, 14)
                self.state = 495
                self.gd_ltl_sometime_after()
                pass

            elif la_ == 15:
                self.enterOuterAlt(localctx, 15)
                self.state = 496
                self.gd_ltl_sometime_before()
                pass

            elif la_ == 16:
                self.enterOuterAlt(localctx, 16)
                self.state = 497
                self.gd_preference()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Gd_emptyContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_gd_empty

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGd_empty" ):
                listener.enterGd_empty(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGd_empty" ):
                listener.exitGd_empty(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGd_empty" ):
                return visitor.visitGd_empty(self)
            else:
                return visitor.visitChildren(self)




    def gd_empty(self):

        localctx = antlrHDDLParser.Gd_emptyContext(self, self._ctx, self.state)
        self.enterRule(localctx, 58, self.RULE_gd_empty)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 500
            self.match(antlrHDDLParser.T__0)
            self.state = 501
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Gd_conjuctionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def gd(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.GdContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.GdContext,i)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_gd_conjuction

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGd_conjuction" ):
                listener.enterGd_conjuction(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGd_conjuction" ):
                listener.exitGd_conjuction(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGd_conjuction" ):
                return visitor.visitGd_conjuction(self)
            else:
                return visitor.visitChildren(self)




    def gd_conjuction(self):

        localctx = antlrHDDLParser.Gd_conjuctionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 60, self.RULE_gd_conjuction)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 503
            self.match(antlrHDDLParser.T__0)
            self.state = 504
            self.match(antlrHDDLParser.T__25)
            self.state = 506 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 505
                self.gd()
                self.state = 508 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==antlrHDDLParser.T__0 or _la==antlrHDDLParser.T__54):
                    break

            self.state = 510
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Gd_disjuctionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def gd(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.GdContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.GdContext,i)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_gd_disjuction

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGd_disjuction" ):
                listener.enterGd_disjuction(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGd_disjuction" ):
                listener.exitGd_disjuction(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGd_disjuction" ):
                return visitor.visitGd_disjuction(self)
            else:
                return visitor.visitChildren(self)




    def gd_disjuction(self):

        localctx = antlrHDDLParser.Gd_disjuctionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 62, self.RULE_gd_disjuction)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 512
            self.match(antlrHDDLParser.T__0)
            self.state = 513
            self.match(antlrHDDLParser.T__33)
            self.state = 515 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 514
                self.gd()
                self.state = 517 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==antlrHDDLParser.T__0 or _la==antlrHDDLParser.T__54):
                    break

            self.state = 519
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Gd_negationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def gd(self):
            return self.getTypedRuleContext(antlrHDDLParser.GdContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_gd_negation

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGd_negation" ):
                listener.enterGd_negation(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGd_negation" ):
                listener.exitGd_negation(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGd_negation" ):
                return visitor.visitGd_negation(self)
            else:
                return visitor.visitChildren(self)




    def gd_negation(self):

        localctx = antlrHDDLParser.Gd_negationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 64, self.RULE_gd_negation)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 521
            self.match(antlrHDDLParser.T__0)
            self.state = 522
            self.match(antlrHDDLParser.T__27)
            self.state = 523
            self.gd()
            self.state = 524
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Gd_implicationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def gd(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.GdContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.GdContext,i)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_gd_implication

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGd_implication" ):
                listener.enterGd_implication(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGd_implication" ):
                listener.exitGd_implication(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGd_implication" ):
                return visitor.visitGd_implication(self)
            else:
                return visitor.visitChildren(self)




    def gd_implication(self):

        localctx = antlrHDDLParser.Gd_implicationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 66, self.RULE_gd_implication)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 526
            self.match(antlrHDDLParser.T__0)
            self.state = 527
            self.match(antlrHDDLParser.T__34)
            self.state = 528
            self.gd()
            self.state = 529
            self.gd()
            self.state = 530
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Gd_existentialContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def typed_var_list(self):
            return self.getTypedRuleContext(antlrHDDLParser.Typed_var_listContext,0)


        def gd(self):
            return self.getTypedRuleContext(antlrHDDLParser.GdContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_gd_existential

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGd_existential" ):
                listener.enterGd_existential(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGd_existential" ):
                listener.exitGd_existential(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGd_existential" ):
                return visitor.visitGd_existential(self)
            else:
                return visitor.visitChildren(self)




    def gd_existential(self):

        localctx = antlrHDDLParser.Gd_existentialContext(self, self._ctx, self.state)
        self.enterRule(localctx, 68, self.RULE_gd_existential)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 532
            self.match(antlrHDDLParser.T__0)
            self.state = 533
            self.match(antlrHDDLParser.T__35)
            self.state = 534
            self.match(antlrHDDLParser.T__0)
            self.state = 535
            self.typed_var_list()
            self.state = 536
            self.match(antlrHDDLParser.T__3)
            self.state = 537
            self.gd()
            self.state = 538
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Gd_universalContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def typed_var_list(self):
            return self.getTypedRuleContext(antlrHDDLParser.Typed_var_listContext,0)


        def gd(self):
            return self.getTypedRuleContext(antlrHDDLParser.GdContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_gd_universal

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGd_universal" ):
                listener.enterGd_universal(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGd_universal" ):
                listener.exitGd_universal(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGd_universal" ):
                return visitor.visitGd_universal(self)
            else:
                return visitor.visitChildren(self)




    def gd_universal(self):

        localctx = antlrHDDLParser.Gd_universalContext(self, self._ctx, self.state)
        self.enterRule(localctx, 70, self.RULE_gd_universal)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 540
            self.match(antlrHDDLParser.T__0)
            self.state = 541
            self.match(antlrHDDLParser.T__36)
            self.state = 542
            self.match(antlrHDDLParser.T__0)
            self.state = 543
            self.typed_var_list()
            self.state = 544
            self.match(antlrHDDLParser.T__3)
            self.state = 545
            self.gd()
            self.state = 546
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Gd_equality_constraintContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def equallity(self):
            return self.getTypedRuleContext(antlrHDDLParser.EquallityContext,0)


        def var_or_const(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.Var_or_constContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.Var_or_constContext,i)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_gd_equality_constraint

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGd_equality_constraint" ):
                listener.enterGd_equality_constraint(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGd_equality_constraint" ):
                listener.exitGd_equality_constraint(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGd_equality_constraint" ):
                return visitor.visitGd_equality_constraint(self)
            else:
                return visitor.visitChildren(self)




    def gd_equality_constraint(self):

        localctx = antlrHDDLParser.Gd_equality_constraintContext(self, self._ctx, self.state)
        self.enterRule(localctx, 72, self.RULE_gd_equality_constraint)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 548
            self.equallity()
            self.state = 549
            self.var_or_const()
            self.state = 550
            self.var_or_const()
            self.state = 551
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Gd_ltl_at_endContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def gd(self):
            return self.getTypedRuleContext(antlrHDDLParser.GdContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_gd_ltl_at_end

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGd_ltl_at_end" ):
                listener.enterGd_ltl_at_end(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGd_ltl_at_end" ):
                listener.exitGd_ltl_at_end(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGd_ltl_at_end" ):
                return visitor.visitGd_ltl_at_end(self)
            else:
                return visitor.visitChildren(self)




    def gd_ltl_at_end(self):

        localctx = antlrHDDLParser.Gd_ltl_at_endContext(self, self._ctx, self.state)
        self.enterRule(localctx, 74, self.RULE_gd_ltl_at_end)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 553
            self.match(antlrHDDLParser.T__0)
            self.state = 554
            self.match(antlrHDDLParser.T__37)
            self.state = 555
            self.gd()
            self.state = 556
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Gd_ltl_alwaysContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def gd(self):
            return self.getTypedRuleContext(antlrHDDLParser.GdContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_gd_ltl_always

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGd_ltl_always" ):
                listener.enterGd_ltl_always(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGd_ltl_always" ):
                listener.exitGd_ltl_always(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGd_ltl_always" ):
                return visitor.visitGd_ltl_always(self)
            else:
                return visitor.visitChildren(self)




    def gd_ltl_always(self):

        localctx = antlrHDDLParser.Gd_ltl_alwaysContext(self, self._ctx, self.state)
        self.enterRule(localctx, 76, self.RULE_gd_ltl_always)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 558
            self.match(antlrHDDLParser.T__0)
            self.state = 559
            self.match(antlrHDDLParser.T__38)
            self.state = 560
            self.gd()
            self.state = 561
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Gd_ltl_sometimeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def gd(self):
            return self.getTypedRuleContext(antlrHDDLParser.GdContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_gd_ltl_sometime

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGd_ltl_sometime" ):
                listener.enterGd_ltl_sometime(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGd_ltl_sometime" ):
                listener.exitGd_ltl_sometime(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGd_ltl_sometime" ):
                return visitor.visitGd_ltl_sometime(self)
            else:
                return visitor.visitChildren(self)




    def gd_ltl_sometime(self):

        localctx = antlrHDDLParser.Gd_ltl_sometimeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 78, self.RULE_gd_ltl_sometime)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 563
            self.match(antlrHDDLParser.T__0)
            self.state = 564
            self.match(antlrHDDLParser.T__39)
            self.state = 565
            self.gd()
            self.state = 566
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Gd_ltl_at_most_onceContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def gd(self):
            return self.getTypedRuleContext(antlrHDDLParser.GdContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_gd_ltl_at_most_once

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGd_ltl_at_most_once" ):
                listener.enterGd_ltl_at_most_once(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGd_ltl_at_most_once" ):
                listener.exitGd_ltl_at_most_once(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGd_ltl_at_most_once" ):
                return visitor.visitGd_ltl_at_most_once(self)
            else:
                return visitor.visitChildren(self)




    def gd_ltl_at_most_once(self):

        localctx = antlrHDDLParser.Gd_ltl_at_most_onceContext(self, self._ctx, self.state)
        self.enterRule(localctx, 80, self.RULE_gd_ltl_at_most_once)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 568
            self.match(antlrHDDLParser.T__0)
            self.state = 569
            self.match(antlrHDDLParser.T__40)
            self.state = 570
            self.gd()
            self.state = 571
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Gd_ltl_sometime_afterContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def gd(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.GdContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.GdContext,i)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_gd_ltl_sometime_after

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGd_ltl_sometime_after" ):
                listener.enterGd_ltl_sometime_after(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGd_ltl_sometime_after" ):
                listener.exitGd_ltl_sometime_after(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGd_ltl_sometime_after" ):
                return visitor.visitGd_ltl_sometime_after(self)
            else:
                return visitor.visitChildren(self)




    def gd_ltl_sometime_after(self):

        localctx = antlrHDDLParser.Gd_ltl_sometime_afterContext(self, self._ctx, self.state)
        self.enterRule(localctx, 82, self.RULE_gd_ltl_sometime_after)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 573
            self.match(antlrHDDLParser.T__0)
            self.state = 574
            self.match(antlrHDDLParser.T__41)
            self.state = 575
            self.gd()
            self.state = 576
            self.gd()
            self.state = 577
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Gd_ltl_sometime_beforeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def gd(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.GdContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.GdContext,i)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_gd_ltl_sometime_before

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGd_ltl_sometime_before" ):
                listener.enterGd_ltl_sometime_before(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGd_ltl_sometime_before" ):
                listener.exitGd_ltl_sometime_before(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGd_ltl_sometime_before" ):
                return visitor.visitGd_ltl_sometime_before(self)
            else:
                return visitor.visitChildren(self)




    def gd_ltl_sometime_before(self):

        localctx = antlrHDDLParser.Gd_ltl_sometime_beforeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 84, self.RULE_gd_ltl_sometime_before)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 579
            self.match(antlrHDDLParser.T__0)
            self.state = 580
            self.match(antlrHDDLParser.T__42)
            self.state = 581
            self.gd()
            self.state = 582
            self.gd()
            self.state = 583
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Gd_preferenceContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NAME(self):
            return self.getToken(antlrHDDLParser.NAME, 0)

        def gd(self):
            return self.getTypedRuleContext(antlrHDDLParser.GdContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_gd_preference

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGd_preference" ):
                listener.enterGd_preference(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGd_preference" ):
                listener.exitGd_preference(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGd_preference" ):
                return visitor.visitGd_preference(self)
            else:
                return visitor.visitChildren(self)




    def gd_preference(self):

        localctx = antlrHDDLParser.Gd_preferenceContext(self, self._ctx, self.state)
        self.enterRule(localctx, 86, self.RULE_gd_preference)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 585
            self.match(antlrHDDLParser.T__0)
            self.state = 586
            self.match(antlrHDDLParser.T__43)
            self.state = 587
            self.match(antlrHDDLParser.NAME)
            self.state = 588
            self.gd()
            self.state = 589
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class EffectContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def eff_empty(self):
            return self.getTypedRuleContext(antlrHDDLParser.Eff_emptyContext,0)


        def eff_conjunction(self):
            return self.getTypedRuleContext(antlrHDDLParser.Eff_conjunctionContext,0)


        def eff_universal(self):
            return self.getTypedRuleContext(antlrHDDLParser.Eff_universalContext,0)


        def eff_conditional(self):
            return self.getTypedRuleContext(antlrHDDLParser.Eff_conditionalContext,0)


        def literal(self):
            return self.getTypedRuleContext(antlrHDDLParser.LiteralContext,0)


        def p_effect(self):
            return self.getTypedRuleContext(antlrHDDLParser.P_effectContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_effect

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEffect" ):
                listener.enterEffect(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEffect" ):
                listener.exitEffect(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitEffect" ):
                return visitor.visitEffect(self)
            else:
                return visitor.visitChildren(self)




    def effect(self):

        localctx = antlrHDDLParser.EffectContext(self, self._ctx, self.state)
        self.enterRule(localctx, 88, self.RULE_effect)
        try:
            self.state = 597
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,41,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 591
                self.eff_empty()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 592
                self.eff_conjunction()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 593
                self.eff_universal()
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 594
                self.eff_conditional()
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 595
                self.literal()
                pass

            elif la_ == 6:
                self.enterOuterAlt(localctx, 6)
                self.state = 596
                self.p_effect()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Eff_emptyContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_eff_empty

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEff_empty" ):
                listener.enterEff_empty(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEff_empty" ):
                listener.exitEff_empty(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitEff_empty" ):
                return visitor.visitEff_empty(self)
            else:
                return visitor.visitChildren(self)




    def eff_empty(self):

        localctx = antlrHDDLParser.Eff_emptyContext(self, self._ctx, self.state)
        self.enterRule(localctx, 90, self.RULE_eff_empty)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 599
            self.match(antlrHDDLParser.T__0)
            self.state = 600
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Eff_conjunctionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def effect(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.EffectContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.EffectContext,i)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_eff_conjunction

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEff_conjunction" ):
                listener.enterEff_conjunction(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEff_conjunction" ):
                listener.exitEff_conjunction(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitEff_conjunction" ):
                return visitor.visitEff_conjunction(self)
            else:
                return visitor.visitChildren(self)




    def eff_conjunction(self):

        localctx = antlrHDDLParser.Eff_conjunctionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 92, self.RULE_eff_conjunction)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 602
            self.match(antlrHDDLParser.T__0)
            self.state = 603
            self.match(antlrHDDLParser.T__25)
            self.state = 605 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 604
                self.effect()
                self.state = 607 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==antlrHDDLParser.T__0):
                    break

            self.state = 609
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Eff_universalContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def typed_var_list(self):
            return self.getTypedRuleContext(antlrHDDLParser.Typed_var_listContext,0)


        def effect(self):
            return self.getTypedRuleContext(antlrHDDLParser.EffectContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_eff_universal

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEff_universal" ):
                listener.enterEff_universal(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEff_universal" ):
                listener.exitEff_universal(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitEff_universal" ):
                return visitor.visitEff_universal(self)
            else:
                return visitor.visitChildren(self)




    def eff_universal(self):

        localctx = antlrHDDLParser.Eff_universalContext(self, self._ctx, self.state)
        self.enterRule(localctx, 94, self.RULE_eff_universal)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 611
            self.match(antlrHDDLParser.T__0)
            self.state = 612
            self.match(antlrHDDLParser.T__36)
            self.state = 613
            self.match(antlrHDDLParser.T__0)
            self.state = 614
            self.typed_var_list()
            self.state = 615
            self.match(antlrHDDLParser.T__3)
            self.state = 616
            self.effect()
            self.state = 617
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Eff_conditionalContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def gd(self):
            return self.getTypedRuleContext(antlrHDDLParser.GdContext,0)


        def effect(self):
            return self.getTypedRuleContext(antlrHDDLParser.EffectContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_eff_conditional

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEff_conditional" ):
                listener.enterEff_conditional(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEff_conditional" ):
                listener.exitEff_conditional(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitEff_conditional" ):
                return visitor.visitEff_conditional(self)
            else:
                return visitor.visitChildren(self)




    def eff_conditional(self):

        localctx = antlrHDDLParser.Eff_conditionalContext(self, self._ctx, self.state)
        self.enterRule(localctx, 96, self.RULE_eff_conditional)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 619
            self.match(antlrHDDLParser.T__0)
            self.state = 620
            self.match(antlrHDDLParser.T__44)
            self.state = 621
            self.gd()
            self.state = 622
            self.effect()
            self.state = 623
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class LiteralContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def neg_atomic_formula(self):
            return self.getTypedRuleContext(antlrHDDLParser.Neg_atomic_formulaContext,0)


        def atomic_formula(self):
            return self.getTypedRuleContext(antlrHDDLParser.Atomic_formulaContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_literal

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLiteral" ):
                listener.enterLiteral(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLiteral" ):
                listener.exitLiteral(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLiteral" ):
                return visitor.visitLiteral(self)
            else:
                return visitor.visitChildren(self)




    def literal(self):

        localctx = antlrHDDLParser.LiteralContext(self, self._ctx, self.state)
        self.enterRule(localctx, 98, self.RULE_literal)
        try:
            self.state = 627
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,43,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 625
                self.neg_atomic_formula()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 626
                self.atomic_formula()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Neg_atomic_formulaContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def atomic_formula(self):
            return self.getTypedRuleContext(antlrHDDLParser.Atomic_formulaContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_neg_atomic_formula

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNeg_atomic_formula" ):
                listener.enterNeg_atomic_formula(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNeg_atomic_formula" ):
                listener.exitNeg_atomic_formula(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNeg_atomic_formula" ):
                return visitor.visitNeg_atomic_formula(self)
            else:
                return visitor.visitChildren(self)




    def neg_atomic_formula(self):

        localctx = antlrHDDLParser.Neg_atomic_formulaContext(self, self._ctx, self.state)
        self.enterRule(localctx, 100, self.RULE_neg_atomic_formula)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 629
            self.match(antlrHDDLParser.T__0)
            self.state = 630
            self.match(antlrHDDLParser.T__27)
            self.state = 631
            self.atomic_formula()
            self.state = 632
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class P_effectContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def assign_op(self):
            return self.getTypedRuleContext(antlrHDDLParser.Assign_opContext,0)


        def f_head(self):
            return self.getTypedRuleContext(antlrHDDLParser.F_headContext,0)


        def f_exp(self):
            return self.getTypedRuleContext(antlrHDDLParser.F_expContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_p_effect

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterP_effect" ):
                listener.enterP_effect(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitP_effect" ):
                listener.exitP_effect(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitP_effect" ):
                return visitor.visitP_effect(self)
            else:
                return visitor.visitChildren(self)




    def p_effect(self):

        localctx = antlrHDDLParser.P_effectContext(self, self._ctx, self.state)
        self.enterRule(localctx, 102, self.RULE_p_effect)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 634
            self.match(antlrHDDLParser.T__0)
            self.state = 635
            self.assign_op()
            self.state = 636
            self.f_head()
            self.state = 637
            self.f_exp()
            self.state = 638
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Assign_opContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_assign_op

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAssign_op" ):
                listener.enterAssign_op(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAssign_op" ):
                listener.exitAssign_op(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAssign_op" ):
                return visitor.visitAssign_op(self)
            else:
                return visitor.visitChildren(self)




    def assign_op(self):

        localctx = antlrHDDLParser.Assign_opContext(self, self._ctx, self.state)
        self.enterRule(localctx, 104, self.RULE_assign_op)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 640
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << antlrHDDLParser.T__45) | (1 << antlrHDDLParser.T__46) | (1 << antlrHDDLParser.T__47) | (1 << antlrHDDLParser.T__48) | (1 << antlrHDDLParser.T__49))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class F_headContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def func_symbol(self):
            return self.getTypedRuleContext(antlrHDDLParser.Func_symbolContext,0)


        def term(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.TermContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.TermContext,i)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_f_head

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterF_head" ):
                listener.enterF_head(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitF_head" ):
                listener.exitF_head(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitF_head" ):
                return visitor.visitF_head(self)
            else:
                return visitor.visitChildren(self)




    def f_head(self):

        localctx = antlrHDDLParser.F_headContext(self, self._ctx, self.state)
        self.enterRule(localctx, 106, self.RULE_f_head)
        self._la = 0 # Token type
        try:
            self.state = 653
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [antlrHDDLParser.NAME]:
                self.enterOuterAlt(localctx, 1)
                self.state = 642
                self.func_symbol()
                pass
            elif token in [antlrHDDLParser.T__0]:
                self.enterOuterAlt(localctx, 2)
                self.state = 643
                self.match(antlrHDDLParser.T__0)
                self.state = 644
                self.func_symbol()
                self.state = 648
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==antlrHDDLParser.T__0 or _la==antlrHDDLParser.VAR_NAME or _la==antlrHDDLParser.NAME:
                    self.state = 645
                    self.term()
                    self.state = 650
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 651
                self.match(antlrHDDLParser.T__3)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class F_expContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NUMBER(self):
            return self.getToken(antlrHDDLParser.NUMBER, 0)

        def bin_op(self):
            return self.getTypedRuleContext(antlrHDDLParser.Bin_opContext,0)


        def f_exp(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.F_expContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.F_expContext,i)


        def multi_op(self):
            return self.getTypedRuleContext(antlrHDDLParser.Multi_opContext,0)


        def f_head(self):
            return self.getTypedRuleContext(antlrHDDLParser.F_headContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_f_exp

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterF_exp" ):
                listener.enterF_exp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitF_exp" ):
                listener.exitF_exp(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitF_exp" ):
                return visitor.visitF_exp(self)
            else:
                return visitor.visitChildren(self)




    def f_exp(self):

        localctx = antlrHDDLParser.F_expContext(self, self._ctx, self.state)
        self.enterRule(localctx, 108, self.RULE_f_exp)
        self._la = 0 # Token type
        try:
            self.state = 678
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,47,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 655
                self.match(antlrHDDLParser.NUMBER)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 656
                self.match(antlrHDDLParser.T__0)
                self.state = 657
                self.bin_op()
                self.state = 658
                self.f_exp()
                self.state = 659
                self.f_exp()
                self.state = 660
                self.match(antlrHDDLParser.T__3)
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 662
                self.match(antlrHDDLParser.T__0)
                self.state = 663
                self.multi_op()
                self.state = 664
                self.f_exp()
                self.state = 666 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 665
                    self.f_exp()
                    self.state = 668 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not (_la==antlrHDDLParser.T__0 or _la==antlrHDDLParser.NAME or _la==antlrHDDLParser.NUMBER):
                        break

                self.state = 670
                self.match(antlrHDDLParser.T__3)
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 672
                self.match(antlrHDDLParser.T__0)
                self.state = 673
                self.match(antlrHDDLParser.T__6)
                self.state = 674
                self.f_exp()
                self.state = 675
                self.match(antlrHDDLParser.T__3)
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 677
                self.f_head()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Bin_opContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def multi_op(self):
            return self.getTypedRuleContext(antlrHDDLParser.Multi_opContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_bin_op

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBin_op" ):
                listener.enterBin_op(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBin_op" ):
                listener.exitBin_op(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBin_op" ):
                return visitor.visitBin_op(self)
            else:
                return visitor.visitChildren(self)




    def bin_op(self):

        localctx = antlrHDDLParser.Bin_opContext(self, self._ctx, self.state)
        self.enterRule(localctx, 110, self.RULE_bin_op)
        try:
            self.state = 683
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [antlrHDDLParser.T__51, antlrHDDLParser.T__52]:
                self.enterOuterAlt(localctx, 1)
                self.state = 680
                self.multi_op()
                pass
            elif token in [antlrHDDLParser.T__6]:
                self.enterOuterAlt(localctx, 2)
                self.state = 681
                self.match(antlrHDDLParser.T__6)
                pass
            elif token in [antlrHDDLParser.T__50]:
                self.enterOuterAlt(localctx, 3)
                self.state = 682
                self.match(antlrHDDLParser.T__50)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Multi_opContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_multi_op

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMulti_op" ):
                listener.enterMulti_op(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMulti_op" ):
                listener.exitMulti_op(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMulti_op" ):
                return visitor.visitMulti_op(self)
            else:
                return visitor.visitChildren(self)




    def multi_op(self):

        localctx = antlrHDDLParser.Multi_opContext(self, self._ctx, self.state)
        self.enterRule(localctx, 112, self.RULE_multi_op)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 685
            _la = self._input.LA(1)
            if not(_la==antlrHDDLParser.T__51 or _la==antlrHDDLParser.T__52):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Atomic_formulaContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def predicate(self):
            return self.getTypedRuleContext(antlrHDDLParser.PredicateContext,0)


        def var_or_const(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.Var_or_constContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.Var_or_constContext,i)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_atomic_formula

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAtomic_formula" ):
                listener.enterAtomic_formula(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAtomic_formula" ):
                listener.exitAtomic_formula(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAtomic_formula" ):
                return visitor.visitAtomic_formula(self)
            else:
                return visitor.visitChildren(self)




    def atomic_formula(self):

        localctx = antlrHDDLParser.Atomic_formulaContext(self, self._ctx, self.state)
        self.enterRule(localctx, 114, self.RULE_atomic_formula)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 687
            self.match(antlrHDDLParser.T__0)
            self.state = 688
            self.predicate()
            self.state = 692
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==antlrHDDLParser.VAR_NAME or _la==antlrHDDLParser.NAME:
                self.state = 689
                self.var_or_const()
                self.state = 694
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 695
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PredicateContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NAME(self):
            return self.getToken(antlrHDDLParser.NAME, 0)

        def getRuleIndex(self):
            return antlrHDDLParser.RULE_predicate

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPredicate" ):
                listener.enterPredicate(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPredicate" ):
                listener.exitPredicate(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPredicate" ):
                return visitor.visitPredicate(self)
            else:
                return visitor.visitChildren(self)




    def predicate(self):

        localctx = antlrHDDLParser.PredicateContext(self, self._ctx, self.state)
        self.enterRule(localctx, 116, self.RULE_predicate)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 697
            self.match(antlrHDDLParser.NAME)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class EquallityContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_equallity

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEquallity" ):
                listener.enterEquallity(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEquallity" ):
                listener.exitEquallity(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitEquallity" ):
                return visitor.visitEquallity(self)
            else:
                return visitor.visitChildren(self)




    def equallity(self):

        localctx = antlrHDDLParser.EquallityContext(self, self._ctx, self.state)
        self.enterRule(localctx, 118, self.RULE_equallity)
        try:
            self.state = 702
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [antlrHDDLParser.T__0]:
                self.enterOuterAlt(localctx, 1)
                self.state = 699
                self.match(antlrHDDLParser.T__0)
                self.state = 700
                self.match(antlrHDDLParser.T__53)
                pass
            elif token in [antlrHDDLParser.T__54]:
                self.enterOuterAlt(localctx, 2)
                self.state = 701
                self.match(antlrHDDLParser.T__54)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Typed_var_listContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def typed_vars(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.Typed_varsContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.Typed_varsContext,i)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_typed_var_list

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTyped_var_list" ):
                listener.enterTyped_var_list(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTyped_var_list" ):
                listener.exitTyped_var_list(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTyped_var_list" ):
                return visitor.visitTyped_var_list(self)
            else:
                return visitor.visitChildren(self)




    def typed_var_list(self):

        localctx = antlrHDDLParser.Typed_var_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 120, self.RULE_typed_var_list)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 707
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==antlrHDDLParser.VAR_NAME:
                self.state = 704
                self.typed_vars()
                self.state = 709
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Typed_obj_listContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def typed_objs(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.Typed_objsContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.Typed_objsContext,i)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_typed_obj_list

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTyped_obj_list" ):
                listener.enterTyped_obj_list(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTyped_obj_list" ):
                listener.exitTyped_obj_list(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTyped_obj_list" ):
                return visitor.visitTyped_obj_list(self)
            else:
                return visitor.visitChildren(self)




    def typed_obj_list(self):

        localctx = antlrHDDLParser.Typed_obj_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 122, self.RULE_typed_obj_list)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 713
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==antlrHDDLParser.NAME:
                self.state = 710
                self.typed_objs()
                self.state = 715
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Typed_varsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def var_type(self):
            return self.getTypedRuleContext(antlrHDDLParser.Var_typeContext,0)


        def VAR_NAME(self, i:int=None):
            if i is None:
                return self.getTokens(antlrHDDLParser.VAR_NAME)
            else:
                return self.getToken(antlrHDDLParser.VAR_NAME, i)

        def getRuleIndex(self):
            return antlrHDDLParser.RULE_typed_vars

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTyped_vars" ):
                listener.enterTyped_vars(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTyped_vars" ):
                listener.exitTyped_vars(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTyped_vars" ):
                return visitor.visitTyped_vars(self)
            else:
                return visitor.visitChildren(self)




    def typed_vars(self):

        localctx = antlrHDDLParser.Typed_varsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 124, self.RULE_typed_vars)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 717 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 716
                self.match(antlrHDDLParser.VAR_NAME)
                self.state = 719 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==antlrHDDLParser.VAR_NAME):
                    break

            self.state = 721
            self.match(antlrHDDLParser.T__6)
            self.state = 722
            self.var_type()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Typed_varContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def VAR_NAME(self):
            return self.getToken(antlrHDDLParser.VAR_NAME, 0)

        def var_type(self):
            return self.getTypedRuleContext(antlrHDDLParser.Var_typeContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_typed_var

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTyped_var" ):
                listener.enterTyped_var(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTyped_var" ):
                listener.exitTyped_var(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTyped_var" ):
                return visitor.visitTyped_var(self)
            else:
                return visitor.visitChildren(self)




    def typed_var(self):

        localctx = antlrHDDLParser.Typed_varContext(self, self._ctx, self.state)
        self.enterRule(localctx, 126, self.RULE_typed_var)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 724
            self.match(antlrHDDLParser.VAR_NAME)
            self.state = 725
            self.match(antlrHDDLParser.T__6)
            self.state = 726
            self.var_type()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Typed_objsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def var_type(self):
            return self.getTypedRuleContext(antlrHDDLParser.Var_typeContext,0)


        def new_consts(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.New_constsContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.New_constsContext,i)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_typed_objs

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTyped_objs" ):
                listener.enterTyped_objs(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTyped_objs" ):
                listener.exitTyped_objs(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTyped_objs" ):
                return visitor.visitTyped_objs(self)
            else:
                return visitor.visitChildren(self)




    def typed_objs(self):

        localctx = antlrHDDLParser.Typed_objsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 128, self.RULE_typed_objs)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 729 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 728
                self.new_consts()
                self.state = 731 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==antlrHDDLParser.NAME):
                    break

            self.state = 733
            self.match(antlrHDDLParser.T__6)
            self.state = 734
            self.var_type()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class New_constsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NAME(self):
            return self.getToken(antlrHDDLParser.NAME, 0)

        def getRuleIndex(self):
            return antlrHDDLParser.RULE_new_consts

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNew_consts" ):
                listener.enterNew_consts(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNew_consts" ):
                listener.exitNew_consts(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNew_consts" ):
                return visitor.visitNew_consts(self)
            else:
                return visitor.visitChildren(self)




    def new_consts(self):

        localctx = antlrHDDLParser.New_constsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 130, self.RULE_new_consts)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 736
            self.match(antlrHDDLParser.NAME)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Var_typeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NAME(self):
            return self.getToken(antlrHDDLParser.NAME, 0)

        def var_type(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.Var_typeContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.Var_typeContext,i)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_var_type

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVar_type" ):
                listener.enterVar_type(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVar_type" ):
                listener.exitVar_type(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVar_type" ):
                return visitor.visitVar_type(self)
            else:
                return visitor.visitChildren(self)




    def var_type(self):

        localctx = antlrHDDLParser.Var_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 132, self.RULE_var_type)
        self._la = 0 # Token type
        try:
            self.state = 748
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [antlrHDDLParser.NAME]:
                self.enterOuterAlt(localctx, 1)
                self.state = 738
                self.match(antlrHDDLParser.NAME)
                pass
            elif token in [antlrHDDLParser.T__0]:
                self.enterOuterAlt(localctx, 2)
                self.state = 739
                self.match(antlrHDDLParser.T__0)
                self.state = 740
                self.match(antlrHDDLParser.T__55)
                self.state = 742 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 741
                    self.var_type()
                    self.state = 744 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not (_la==antlrHDDLParser.T__0 or _la==antlrHDDLParser.NAME):
                        break

                self.state = 746
                self.match(antlrHDDLParser.T__3)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Var_or_constContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NAME(self):
            return self.getToken(antlrHDDLParser.NAME, 0)

        def VAR_NAME(self):
            return self.getToken(antlrHDDLParser.VAR_NAME, 0)

        def getRuleIndex(self):
            return antlrHDDLParser.RULE_var_or_const

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVar_or_const" ):
                listener.enterVar_or_const(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVar_or_const" ):
                listener.exitVar_or_const(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVar_or_const" ):
                return visitor.visitVar_or_const(self)
            else:
                return visitor.visitChildren(self)




    def var_or_const(self):

        localctx = antlrHDDLParser.Var_or_constContext(self, self._ctx, self.state)
        self.enterRule(localctx, 134, self.RULE_var_or_const)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 750
            _la = self._input.LA(1)
            if not(_la==antlrHDDLParser.VAR_NAME or _la==antlrHDDLParser.NAME):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TermContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NAME(self):
            return self.getToken(antlrHDDLParser.NAME, 0)

        def VAR_NAME(self):
            return self.getToken(antlrHDDLParser.VAR_NAME, 0)

        def functionterm(self):
            return self.getTypedRuleContext(antlrHDDLParser.FunctiontermContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_term

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTerm" ):
                listener.enterTerm(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTerm" ):
                listener.exitTerm(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTerm" ):
                return visitor.visitTerm(self)
            else:
                return visitor.visitChildren(self)




    def term(self):

        localctx = antlrHDDLParser.TermContext(self, self._ctx, self.state)
        self.enterRule(localctx, 136, self.RULE_term)
        try:
            self.state = 755
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [antlrHDDLParser.NAME]:
                self.enterOuterAlt(localctx, 1)
                self.state = 752
                self.match(antlrHDDLParser.NAME)
                pass
            elif token in [antlrHDDLParser.VAR_NAME]:
                self.enterOuterAlt(localctx, 2)
                self.state = 753
                self.match(antlrHDDLParser.VAR_NAME)
                pass
            elif token in [antlrHDDLParser.T__0]:
                self.enterOuterAlt(localctx, 3)
                self.state = 754
                self.functionterm()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FunctiontermContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def func_symbol(self):
            return self.getTypedRuleContext(antlrHDDLParser.Func_symbolContext,0)


        def term(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.TermContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.TermContext,i)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_functionterm

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFunctionterm" ):
                listener.enterFunctionterm(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFunctionterm" ):
                listener.exitFunctionterm(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFunctionterm" ):
                return visitor.visitFunctionterm(self)
            else:
                return visitor.visitChildren(self)




    def functionterm(self):

        localctx = antlrHDDLParser.FunctiontermContext(self, self._ctx, self.state)
        self.enterRule(localctx, 138, self.RULE_functionterm)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 757
            self.match(antlrHDDLParser.T__0)
            self.state = 758
            self.func_symbol()
            self.state = 762
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==antlrHDDLParser.T__0 or _la==antlrHDDLParser.VAR_NAME or _la==antlrHDDLParser.NAME:
                self.state = 759
                self.term()
                self.state = 764
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 765
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Func_symbolContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NAME(self):
            return self.getToken(antlrHDDLParser.NAME, 0)

        def getRuleIndex(self):
            return antlrHDDLParser.RULE_func_symbol

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFunc_symbol" ):
                listener.enterFunc_symbol(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFunc_symbol" ):
                listener.exitFunc_symbol(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFunc_symbol" ):
                return visitor.visitFunc_symbol(self)
            else:
                return visitor.visitChildren(self)




    def func_symbol(self):

        localctx = antlrHDDLParser.Func_symbolContext(self, self._ctx, self.state)
        self.enterRule(localctx, 140, self.RULE_func_symbol)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 767
            self.match(antlrHDDLParser.NAME)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ProblemContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def NAME(self, i:int=None):
            if i is None:
                return self.getTokens(antlrHDDLParser.NAME)
            else:
                return self.getToken(antlrHDDLParser.NAME, i)

        def p_init(self):
            return self.getTypedRuleContext(antlrHDDLParser.P_initContext,0)


        def require_def(self):
            return self.getTypedRuleContext(antlrHDDLParser.Require_defContext,0)


        def p_object_declaration(self):
            return self.getTypedRuleContext(antlrHDDLParser.P_object_declarationContext,0)


        def p_htn(self):
            return self.getTypedRuleContext(antlrHDDLParser.P_htnContext,0)


        def p_goal(self):
            return self.getTypedRuleContext(antlrHDDLParser.P_goalContext,0)


        def p_constraint(self):
            return self.getTypedRuleContext(antlrHDDLParser.P_constraintContext,0)


        def metric_spec(self):
            return self.getTypedRuleContext(antlrHDDLParser.Metric_specContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_problem

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProblem" ):
                listener.enterProblem(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProblem" ):
                listener.exitProblem(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProblem" ):
                return visitor.visitProblem(self)
            else:
                return visitor.visitChildren(self)




    def problem(self):

        localctx = antlrHDDLParser.ProblemContext(self, self._ctx, self.state)
        self.enterRule(localctx, 142, self.RULE_problem)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 769
            self.match(antlrHDDLParser.T__0)
            self.state = 770
            self.match(antlrHDDLParser.T__1)
            self.state = 771
            self.match(antlrHDDLParser.T__0)
            self.state = 772
            self.match(antlrHDDLParser.T__56)
            self.state = 773
            self.match(antlrHDDLParser.NAME)
            self.state = 774
            self.match(antlrHDDLParser.T__3)
            self.state = 775
            self.match(antlrHDDLParser.T__0)
            self.state = 776
            self.match(antlrHDDLParser.T__57)
            self.state = 777
            self.match(antlrHDDLParser.NAME)
            self.state = 778
            self.match(antlrHDDLParser.T__3)
            self.state = 780
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,59,self._ctx)
            if la_ == 1:
                self.state = 779
                self.require_def()


            self.state = 783
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,60,self._ctx)
            if la_ == 1:
                self.state = 782
                self.p_object_declaration()


            self.state = 786
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,61,self._ctx)
            if la_ == 1:
                self.state = 785
                self.p_htn()


            self.state = 788
            self.p_init()
            self.state = 790
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,62,self._ctx)
            if la_ == 1:
                self.state = 789
                self.p_goal()


            self.state = 793
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,63,self._ctx)
            if la_ == 1:
                self.state = 792
                self.p_constraint()


            self.state = 796
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==antlrHDDLParser.T__0:
                self.state = 795
                self.metric_spec()


            self.state = 798
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class P_object_declarationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def typed_obj_list(self):
            return self.getTypedRuleContext(antlrHDDLParser.Typed_obj_listContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_p_object_declaration

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterP_object_declaration" ):
                listener.enterP_object_declaration(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitP_object_declaration" ):
                listener.exitP_object_declaration(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitP_object_declaration" ):
                return visitor.visitP_object_declaration(self)
            else:
                return visitor.visitChildren(self)




    def p_object_declaration(self):

        localctx = antlrHDDLParser.P_object_declarationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 144, self.RULE_p_object_declaration)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 800
            self.match(antlrHDDLParser.T__0)
            self.state = 801
            self.match(antlrHDDLParser.T__58)
            self.state = 802
            self.typed_obj_list()
            self.state = 803
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class P_initContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def init_el(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.Init_elContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.Init_elContext,i)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_p_init

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterP_init" ):
                listener.enterP_init(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitP_init" ):
                listener.exitP_init(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitP_init" ):
                return visitor.visitP_init(self)
            else:
                return visitor.visitChildren(self)




    def p_init(self):

        localctx = antlrHDDLParser.P_initContext(self, self._ctx, self.state)
        self.enterRule(localctx, 146, self.RULE_p_init)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 805
            self.match(antlrHDDLParser.T__0)
            self.state = 806
            self.match(antlrHDDLParser.T__59)
            self.state = 810
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==antlrHDDLParser.T__0 or _la==antlrHDDLParser.T__54:
                self.state = 807
                self.init_el()
                self.state = 812
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 813
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Init_elContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def literal(self):
            return self.getTypedRuleContext(antlrHDDLParser.LiteralContext,0)


        def num_init(self):
            return self.getTypedRuleContext(antlrHDDLParser.Num_initContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_init_el

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInit_el" ):
                listener.enterInit_el(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInit_el" ):
                listener.exitInit_el(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInit_el" ):
                return visitor.visitInit_el(self)
            else:
                return visitor.visitChildren(self)




    def init_el(self):

        localctx = antlrHDDLParser.Init_elContext(self, self._ctx, self.state)
        self.enterRule(localctx, 148, self.RULE_init_el)
        try:
            self.state = 817
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,66,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 815
                self.literal()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 816
                self.num_init()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Num_initContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def equallity(self):
            return self.getTypedRuleContext(antlrHDDLParser.EquallityContext,0)


        def f_head(self):
            return self.getTypedRuleContext(antlrHDDLParser.F_headContext,0)


        def NUMBER(self):
            return self.getToken(antlrHDDLParser.NUMBER, 0)

        def getRuleIndex(self):
            return antlrHDDLParser.RULE_num_init

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNum_init" ):
                listener.enterNum_init(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNum_init" ):
                listener.exitNum_init(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNum_init" ):
                return visitor.visitNum_init(self)
            else:
                return visitor.visitChildren(self)




    def num_init(self):

        localctx = antlrHDDLParser.Num_initContext(self, self._ctx, self.state)
        self.enterRule(localctx, 150, self.RULE_num_init)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 819
            self.equallity()
            self.state = 820
            self.f_head()
            self.state = 821
            self.match(antlrHDDLParser.NUMBER)
            self.state = 822
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class P_goalContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def gd(self):
            return self.getTypedRuleContext(antlrHDDLParser.GdContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_p_goal

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterP_goal" ):
                listener.enterP_goal(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitP_goal" ):
                listener.exitP_goal(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitP_goal" ):
                return visitor.visitP_goal(self)
            else:
                return visitor.visitChildren(self)




    def p_goal(self):

        localctx = antlrHDDLParser.P_goalContext(self, self._ctx, self.state)
        self.enterRule(localctx, 152, self.RULE_p_goal)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 824
            self.match(antlrHDDLParser.T__0)
            self.state = 825
            self.match(antlrHDDLParser.T__60)
            self.state = 826
            self.gd()
            self.state = 827
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class P_htnContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def tasknetwork_def(self):
            return self.getTypedRuleContext(antlrHDDLParser.Tasknetwork_defContext,0)


        def typed_var_list(self):
            return self.getTypedRuleContext(antlrHDDLParser.Typed_var_listContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_p_htn

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterP_htn" ):
                listener.enterP_htn(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitP_htn" ):
                listener.exitP_htn(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitP_htn" ):
                return visitor.visitP_htn(self)
            else:
                return visitor.visitChildren(self)




    def p_htn(self):

        localctx = antlrHDDLParser.P_htnContext(self, self._ctx, self.state)
        self.enterRule(localctx, 154, self.RULE_p_htn)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 829
            self.match(antlrHDDLParser.T__0)
            self.state = 830
            _la = self._input.LA(1)
            if not(_la==antlrHDDLParser.T__61 or _la==antlrHDDLParser.T__62):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
            self.state = 836
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==antlrHDDLParser.T__12:
                self.state = 831
                self.match(antlrHDDLParser.T__12)
                self.state = 832
                self.match(antlrHDDLParser.T__0)
                self.state = 833
                self.typed_var_list()
                self.state = 834
                self.match(antlrHDDLParser.T__3)


            self.state = 838
            self.tasknetwork_def()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Metric_specContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def optimization(self):
            return self.getTypedRuleContext(antlrHDDLParser.OptimizationContext,0)


        def ground_f_exp(self):
            return self.getTypedRuleContext(antlrHDDLParser.Ground_f_expContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_metric_spec

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMetric_spec" ):
                listener.enterMetric_spec(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMetric_spec" ):
                listener.exitMetric_spec(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMetric_spec" ):
                return visitor.visitMetric_spec(self)
            else:
                return visitor.visitChildren(self)




    def metric_spec(self):

        localctx = antlrHDDLParser.Metric_specContext(self, self._ctx, self.state)
        self.enterRule(localctx, 156, self.RULE_metric_spec)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 840
            self.match(antlrHDDLParser.T__0)
            self.state = 841
            self.match(antlrHDDLParser.T__63)
            self.state = 842
            self.optimization()
            self.state = 843
            self.ground_f_exp()
            self.state = 844
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class OptimizationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_optimization

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterOptimization" ):
                listener.enterOptimization(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitOptimization" ):
                listener.exitOptimization(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitOptimization" ):
                return visitor.visitOptimization(self)
            else:
                return visitor.visitChildren(self)




    def optimization(self):

        localctx = antlrHDDLParser.OptimizationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 158, self.RULE_optimization)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 846
            _la = self._input.LA(1)
            if not(_la==antlrHDDLParser.T__64 or _la==antlrHDDLParser.T__65):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Ground_f_expContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def bin_op(self):
            return self.getTypedRuleContext(antlrHDDLParser.Bin_opContext,0)


        def ground_f_exp(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(antlrHDDLParser.Ground_f_expContext)
            else:
                return self.getTypedRuleContext(antlrHDDLParser.Ground_f_expContext,i)


        def multi_op(self):
            return self.getTypedRuleContext(antlrHDDLParser.Multi_opContext,0)


        def NUMBER(self):
            return self.getToken(antlrHDDLParser.NUMBER, 0)

        def func_symbol(self):
            return self.getTypedRuleContext(antlrHDDLParser.Func_symbolContext,0)


        def NAME(self, i:int=None):
            if i is None:
                return self.getTokens(antlrHDDLParser.NAME)
            else:
                return self.getToken(antlrHDDLParser.NAME, i)

        def getRuleIndex(self):
            return antlrHDDLParser.RULE_ground_f_exp

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGround_f_exp" ):
                listener.enterGround_f_exp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGround_f_exp" ):
                listener.exitGround_f_exp(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitGround_f_exp" ):
                return visitor.visitGround_f_exp(self)
            else:
                return visitor.visitChildren(self)




    def ground_f_exp(self):

        localctx = antlrHDDLParser.Ground_f_expContext(self, self._ctx, self.state)
        self.enterRule(localctx, 160, self.RULE_ground_f_exp)
        self._la = 0 # Token type
        try:
            self.state = 885
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,71,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 848
                self.match(antlrHDDLParser.T__0)
                self.state = 849
                self.bin_op()
                self.state = 850
                self.ground_f_exp()
                self.state = 851
                self.ground_f_exp()
                self.state = 852
                self.match(antlrHDDLParser.T__3)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 854
                self.match(antlrHDDLParser.T__0)
                self.state = 855
                self.multi_op()
                self.state = 856
                self.ground_f_exp()
                self.state = 858 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 857
                    self.ground_f_exp()
                    self.state = 860 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not (_la==antlrHDDLParser.T__0 or ((((_la - 67)) & ~0x3f) == 0 and ((1 << (_la - 67)) & ((1 << (antlrHDDLParser.T__66 - 67)) | (1 << (antlrHDDLParser.T__67 - 67)) | (1 << (antlrHDDLParser.NAME - 67)) | (1 << (antlrHDDLParser.NUMBER - 67)))) != 0)):
                        break

                self.state = 862
                self.match(antlrHDDLParser.T__3)
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 867
                self._errHandler.sync(self)
                token = self._input.LA(1)
                if token in [antlrHDDLParser.T__0]:
                    self.state = 864
                    self.match(antlrHDDLParser.T__0)
                    self.state = 865
                    self.match(antlrHDDLParser.T__6)
                    pass
                elif token in [antlrHDDLParser.T__66]:
                    self.state = 866
                    self.match(antlrHDDLParser.T__66)
                    pass
                else:
                    raise NoViableAltException(self)

                self.state = 869
                self.ground_f_exp()
                self.state = 870
                self.match(antlrHDDLParser.T__3)
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 872
                self.match(antlrHDDLParser.NUMBER)
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 873
                self.match(antlrHDDLParser.T__0)
                self.state = 874
                self.func_symbol()
                self.state = 878
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==antlrHDDLParser.NAME:
                    self.state = 875
                    self.match(antlrHDDLParser.NAME)
                    self.state = 880
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 881
                self.match(antlrHDDLParser.T__3)
                pass

            elif la_ == 6:
                self.enterOuterAlt(localctx, 6)
                self.state = 883
                self.match(antlrHDDLParser.T__67)
                pass

            elif la_ == 7:
                self.enterOuterAlt(localctx, 7)
                self.state = 884
                self.func_symbol()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class P_constraintContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def gd(self):
            return self.getTypedRuleContext(antlrHDDLParser.GdContext,0)


        def getRuleIndex(self):
            return antlrHDDLParser.RULE_p_constraint

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterP_constraint" ):
                listener.enterP_constraint(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitP_constraint" ):
                listener.exitP_constraint(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitP_constraint" ):
                return visitor.visitP_constraint(self)
            else:
                return visitor.visitChildren(self)




    def p_constraint(self):

        localctx = antlrHDDLParser.P_constraintContext(self, self._ctx, self.state)
        self.enterRule(localctx, 162, self.RULE_p_constraint)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 887
            self.match(antlrHDDLParser.T__0)
            self.state = 888
            self.match(antlrHDDLParser.T__22)
            self.state = 889
            self.gd()
            self.state = 890
            self.match(antlrHDDLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





